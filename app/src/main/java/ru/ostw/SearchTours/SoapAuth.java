package ru.ostw.SearchTours;

import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

public class SoapAuth {
	
	public static final String NAMESPACE = "urn:SletatRu:Contracts:Soap11Gate:v1";
	public static final String AUTH_NAMESPACE = "urn:SletatRu:DataTypes:AuthData:v1";

	public static final String URL = "http://module.sletat.ru/XmlGate.svc";
	
    public static Element buildAuthHeader() {
    	Element h = new Element().createElement(AUTH_NAMESPACE, "AuthInfo");
    	
    	Element login = new Element().createElement(AUTH_NAMESPACE, "Login");
    	login.addChild(Node.TEXT, "ostw2@mail.ru");
    	h.addChild(Node.ELEMENT, login);
    	
    	Element password = new Element().createElement(AUTH_NAMESPACE, "Password");
    	password.addChild(Node.TEXT, "19802007JULIA");
    	h.addChild(Node.ELEMENT, password);
    	
    	return h;
    }
}