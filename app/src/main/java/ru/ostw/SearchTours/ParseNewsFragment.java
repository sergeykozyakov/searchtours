package ru.ostw.SearchTours;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

public class ParseNewsFragment extends Fragment {
	
	private static final String HTTP_ADDR = "http://ost-w.ru";
	
	private Callbacks mCallbacks;
	private HttpTask mTask;
	
	public static interface Callbacks {
		void onPreExecute();
		void onProgressUpdate(String... data);
		void onPostExecute(String error);
	}
	
	public static ParseNewsFragment newInstance() {
        return new ParseNewsFragment();
    }
	
	public void setCallbacks(Callbacks callbacks) {
		mCallbacks = callbacks;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mCallbacks = null;
	}
	
	public void execute(String... params) {
		mTask = new HttpTask();
		mTask.execute(params);
	}
	
	public boolean isTaskRunning() {
		if (mTask != null) {
			if (!mTask.isCancelled() && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				return true;
			}
		}
		return false;
	}
	
	private class HttpTask extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (mCallbacks != null) {
				mCallbacks.onPreExecute();
			}
		}
		
		@Override
		protected String doInBackground(String... params) {
			String str;
	      	str = getDataFromURL(params[0], params[1]);
	        
	      	return str;
		}
		
	    @Override
	    protected void onProgressUpdate(String... data) {
	    	super.onProgressUpdate(data);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onProgressUpdate(data);
	    	}
	    }
	    
	    @Override
	    protected void onPostExecute(String error) {
	    	super.onPostExecute(error);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onPostExecute(error);
	    	}
	    }
	    
  	  	// Загрузить html по адресу
        private String getDataFromURL(String address, String width) {
        	String shtml = "";
        	String title = "Текст новости";

        	// Соберем url
        	if (address.startsWith("http")) {
        	}
        	else {
        		address = HTTP_ADDR + address;
        	}
        	
        	URL urlSite = null;
    		try {
    			urlSite = new URL(address);
    		}
    		catch (MalformedURLException e1) {
    			return "Не удалось получить текст новости!";
    		}  
    		
    		// Загрузим данные из потока
        	BufferedReader br;
        	String s = null;
        	
        	try {
    			br = new BufferedReader(new InputStreamReader(urlSite.openConnection().getInputStream(), "windows-1251"));
    	    	while((s = br.readLine()) != null){
    	    	   shtml += s;
    	    	}
    	    	br.close();
    		}
    		catch (IOException e) {
    			return "Не удалось получить текст новости!";
    		}  
    		
    		// Обрабатываем Html
    		
    		// список новстей
    		int indexStart = shtml.indexOf("<div class=\"action-list\">");
    		int indexEnd = shtml.indexOf("<font class=\"text\">Новости");
    		
    		// карточка новости
    		int indexStartNews = shtml.indexOf("<div class=\"news-detail\">");
    		int indexEndNews = shtml.indexOf("<p><a href=\"/novosti/\">Возврат к списку</a></p>");
    		
    		// карточка акции
    		int indexStartActNews = shtml.indexOf("<div class=\"action-detail\">");
    		int indexEndActNews = shtml.indexOf("<p><a href=\"/action/\">Возврат к списку</a></p>");
    		
    		if (indexStartNews < 0 && indexStartActNews > 0) {
    			indexStartNews = indexStartActNews;
    		}
    		if (indexEndNews < 0 && indexEndActNews > 0) {
    			indexEndNews = indexEndActNews;
    		}
    		
    		if (indexStart > 0 && indexEnd > 0 && indexStart < indexEnd) {
    			shtml = shtml.substring(indexStart, indexEnd);
    		}
    		
    		if (indexStartNews > 0 && indexEndNews > 0 && indexStartNews < indexEndNews) {
    			shtml = shtml.substring(indexStartNews, indexEndNews);
    			
    			// получить название
    			int indexStartHeaderNews = shtml.indexOf("<h3>");
        		int indexEndHeaderNews = shtml.indexOf("</h3>");
        		
        		if (indexStartHeaderNews > 0 && indexEndHeaderNews > 0 && indexStartHeaderNews < indexEndHeaderNews) {
        			title = shtml.substring(indexStartHeaderNews + 4, indexEndHeaderNews);
        		}
    			
        		// замена даты на заголовок
    			shtml = shtml.replaceAll("<span class=\"news-date-time\">","<h3>");
    			shtml = shtml.replaceAll("</span>","</h3>");
    			
    			// замена цвета фона
    			shtml = shtml.replaceAll("<font color=\"#ffffff\">","<font color=\"#5d5d5d\">");
    		}
    		
    		try {
    			URLEncoder.encode(shtml, "utf-8").replaceAll("\\+"," ");
    		}
    		catch (Exception e) {
    			return "Не удалось получить текст новости!";
    		}
    	
    		// Добавим стиль для приведения в виде приложения
    		String applicationStyle = "<style>img{height: auto;max-width: 100%;}h2,h3{background:#21697f;color:#ffffff;padding:4px;margin-top:0px;margin-bottom:0px;}body{background:#d1edf9;}a{text-decoration:none;}";
    		applicationStyle += "div.action-item{margin:0 0 30px 0; border:0px solid #ccc;}";
    		
    		applicationStyle += "h3{padding-bottom:8px;}span{color:#5d5d5d;}";
    		
    		applicationStyle += ".action-preview-picture{float:left;width:100px; margin:0 10px 0 0;}.action-preview-text{width:" + width + "px;float:right;}";
    		applicationStyle += ".clear{clear:both;display:block;}";

    		applicationStyle += "</style><body>";
    		
    		shtml = applicationStyle + shtml;
    		
    		publishProgress(shtml, title);
    		return null;
    	}
	}
}