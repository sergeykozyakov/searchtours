package ru.ostw.SearchTours;

import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.telephony.TelephonyManager;

public class MyDevice {
	
	public static final String APP_PREFS = "settings";
	public static final String APP_PREFS_CITIES_IDS = "cities_ids";
	public static final String APP_PREFS_CITIES_NAMES = "cities_names";
	
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for(;;) {
              int count = is.read(bytes, 0, buffer_size);
              if(count == -1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex) {}
    }
    
    public static boolean isTelephonyEnabled(Context context) {
        TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
            return false;
        }
        else {
            return true;
        }
    }
}