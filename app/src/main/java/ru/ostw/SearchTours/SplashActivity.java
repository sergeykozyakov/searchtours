package ru.ostw.SearchTours;

import java.util.ArrayList;
import org.json.JSONArray;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

public class SplashActivity extends Activity {
	
	private SharedPreferences mSettings;
	private GetDepartCitiesFragment mGetDepartCitiesFragment;
	
	private ArrayList<String> mDepartCitiesIds;
	private ArrayList<String> mDepartCitiesNames;
	
	private final GetDepartCitiesFragment.Callbacks getDepartCitiesCallbacks = new GetDepartCitiesFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			mDepartCitiesIds.clear();
			mDepartCitiesNames.clear();
			
			Editor editor = mSettings.edit();
			editor.remove(MyDevice.APP_PREFS_CITIES_IDS);
			editor.remove(MyDevice.APP_PREFS_CITIES_NAMES);
			editor.apply();
		}

		@Override
		public void onProgressUpdate(String... data) {
			mDepartCitiesIds.add(data[0]);
			mDepartCitiesNames.add(data[1]);
		}

		@Override
		public void onPostExecute(String error) {
			if (error != null) {
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 		finish();
		 	}
			else {
				Editor editor = mSettings.edit();
				editor.putString(MyDevice.APP_PREFS_CITIES_IDS, new JSONArray(mDepartCitiesIds).toString());
				editor.putString(MyDevice.APP_PREFS_CITIES_NAMES, new JSONArray(mDepartCitiesNames).toString());
				editor.apply();
				
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
        		finish(); 
			}
		}
	};
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
	    mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
        
        FragmentManager fm = getFragmentManager();
		mGetDepartCitiesFragment = (GetDepartCitiesFragment)fm.findFragmentByTag("depart_cities_fragment");
		
		if (mGetDepartCitiesFragment == null) {
			mGetDepartCitiesFragment = GetDepartCitiesFragment.newInstance();
			fm.beginTransaction().add(mGetDepartCitiesFragment, "depart_cities_fragment").commit();
		}
		
		mGetDepartCitiesFragment.setCallbacks(getDepartCitiesCallbacks);
		
		mDepartCitiesIds = new ArrayList<String>();
        mDepartCitiesNames = new ArrayList<String>();
		
        mGetDepartCitiesFragment.execute();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}