package ru.ostw.SearchTours;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class OrderTourActivity extends FragmentActivity {
	
	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	
	private GetCountriesFragment mGetCountriesFragment;
	private GetCitiesFragment mGetCitiesFragment;
	private SendMailFragment mSendMailFragment;
	
	private ArrayList<String> mDepartCitiesIds;
	private ArrayList<String> mCountriesIds;
	private ArrayList<String> mCitiesIds;
	
	private ArrayList<String> mCitiesNames;
	private ArrayList<Boolean> mCitiesBools;
	
	private ArrayAdapter<String> mDepartCitiesAdapter;
	private ArrayAdapter<String> mCountriesAdapter;
	private ArrayAdapter<String> mNightsAdapter;
	private ArrayAdapter<String> mAdultsAdapter;
	private ArrayAdapter<String> mChildrenAdapter;
	
	private int mDepartCityInd = -1;
	private int mCountryInd = -1;
	private int mAdultInd = 1;
	private int mChildrenInd = 0;
	
	private EditText mEditOrderTourFullName;
	private EditText mEditOrderTourContactPhone;
	private EditText mEditOrderTourDepartCity;
	private EditText mEditOrderTourCountry;
	private EditText mEditOrderTourCity;
	private EditText mEditOrderTourDateFrom;
	private EditText mEditOrderTourDateTo;
	private EditText mEditOrderTourNightsFrom;
	private EditText mEditOrderTourNightsTo;
	private EditText mEditOrderTourAdults;
	private EditText mEditOrderTourChildren;
	
	private Button mButtonOrderTourSubmit;
	
	// Слушатель нажатия на поля с датами
	private final OnClickListener mEditDatesClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			DatePickerFragment fragment = new DatePickerFragment();
			String date = "";
			OnDateSetListener callback = null;
			
			switch (v.getId()) {
				case R.id.editOrderTourDateFrom:
					date = mEditOrderTourDateFrom.getText().toString();
					callback = mDateFromCallback;
					break;
				case R.id.editOrderTourDateTo:
					date = mEditOrderTourDateTo.getText().toString();
					callback = mDateToCallback;
					break;
			}
			
			String[] parts = date.split("\\.");
			Bundle args = new Bundle();
			
	    	if (parts.length == 3) {
				args.putInt("year", Integer.parseInt(parts[2]));
				args.putInt("month", Integer.parseInt(parts[1])-1);
				args.putInt("day", Integer.parseInt(parts[0]));
			}
			else {
				Calendar calendar = Calendar.getInstance();
				
				switch (v.getId()) {
					case R.id.editOrderTourDateFrom:
						calendar.add(Calendar.DAY_OF_MONTH, 1);
						break;
					case R.id.editOrderTourDateTo:
						calendar.add(Calendar.DAY_OF_MONTH, 10);
						break;
				}
				
				args.putInt("year", calendar.get(Calendar.YEAR));
				args.putInt("month", calendar.get(Calendar.MONTH));
				args.putInt("day", calendar.get(Calendar.DAY_OF_MONTH));
			}
			
			fragment.setArguments(args);
			fragment.setCallBack(callback);
			fragment.show(getFragmentManager(), "datePicker");
		}
	};
	
	// Слушатель нажатия на изменение поля "Дата вылета с"
    private OnDateSetListener mDateFromCallback = new OnDateSetListener() {
    	@Override
    	public void onDateSet(DatePicker view, int year, int month, int day) {
    		mEditOrderTourDateFrom.setText(buildDateString(year, month, day));
    	}
    };
    
	// Слушатель нажатия на изменение поля "Дата вылета по"
    private OnDateSetListener mDateToCallback = new OnDateSetListener() {
    	@Override
    	public void onDateSet(DatePicker view, int year, int month, int day) {
    		mEditOrderTourDateTo.setText(buildDateString(year, month, day));
    	}
    };
    
	// Слушатель нажатия на поля с требуемым диалогом
	private final OnClickListener mEditDialogClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			showEditDialog(v.getId());
		}
	};
	
	// Слушатель нажатия на поля с требуемым диалогом
	private final OnClickListener mEditMultiDialogClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			showEditMultiDialog(v.getId());
		}
	};
	
	// Слушатель нажатия на кнопку "Найти"
	private final OnClickListener mButtonOrderTourSubmitClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mEditOrderTourFullName.length() == 0 || mEditOrderTourContactPhone.length() == 0) {
				Toast.makeText(getApplicationContext(), "Укажите свои контактные данные!", Toast.LENGTH_SHORT).show();
				return;
			}			
			
			if (mEditOrderTourDepartCity.length() == 0) {
				Toast.makeText(getApplicationContext(), "Укажите город вылета для выполнения заказа тура!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (mEditOrderTourCountry.length() == 0) {
				Toast.makeText(getApplicationContext(), "Укажите страну прилёта для выполнения заказа тура!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			Calendar now = Calendar.getInstance();
			Calendar dateFrom = Calendar.getInstance();
			Calendar dateTo = Calendar.getInstance();
			
			now.set(Calendar.HOUR, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			
			String[] partsFrom = mEditOrderTourDateFrom.getText().toString().split("\\.");
			dateFrom.set(Integer.valueOf(partsFrom[2]), Integer.valueOf(partsFrom[1])-1, Integer.valueOf(partsFrom[0]));
			
			String[] partsTo = mEditOrderTourDateTo.getText().toString().split("\\.");
			dateTo.set(Integer.valueOf(partsTo[2]), Integer.valueOf(partsTo[1])-1, Integer.valueOf(partsTo[0]));
			
			if (dateTo.getTimeInMillis() < dateFrom.getTimeInMillis()) {
				Toast.makeText(getApplicationContext(), "Конечная дата вылета не может быть меньше начальной даты вылета!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (now.getTimeInMillis() > dateFrom.getTimeInMillis()) {
				Toast.makeText(getApplicationContext(), "Начальная дата вылета не может быть раньше сегодняшнего дня!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Отправка запроса по почте
			mSendMailFragment.execute("Новая заявка на тур из мобильного приложения",   
				"Поступила заявка на побор тура из мобильного приложения Android.\n\n" +
				"ФИО: " + mEditOrderTourFullName.getText().toString() + "\n" +
				"Контактный телефон: " + mEditOrderTourContactPhone.getText().toString() + "\n\n" +
				"Город вылета: " + mEditOrderTourDepartCity.getText().toString() + "\n" +
				"Страна прилёта: " + mEditOrderTourCountry.getText().toString() + "\n" +
				"Курорт(ы): " + mEditOrderTourCity.getText().toString() + "\n" +
				"Даты вылета: c " + mEditOrderTourDateFrom.getText().toString() + " по " + mEditOrderTourDateTo.getText().toString() + "\n" +
				"Ночей: от " + mEditOrderTourNightsFrom.getText().toString() + " до " + mEditOrderTourNightsTo.getText().toString() + "\n" +
				mEditOrderTourAdults.getText().toString() + ", " + mEditOrderTourChildren.getText().toString());
		}
	};
	
	private final GetCountriesFragment.Callbacks mGetCountriesCallbacks = new GetCountriesFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			// Чистим текущее поле
			mCountriesIds.clear();
			mCountriesAdapter.clear();
			
			mCountryInd = -1;
			mEditOrderTourCountry.setText("");
			
			// Чистим зависимые поля
			mCitiesBools.clear();
			mEditOrderTourCity.setText("");
			mEditOrderTourCity.setEnabled(false);
		}

		@Override
		public void onProgressUpdate(String... data) {
			mCountriesIds.add(data[0]);
			mCountriesAdapter.add(data[1]);
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				mEditOrderTourCountry.setEnabled(true);
			}
		}
	};
	
	private final GetCitiesFragment.Callbacks mGetCitiesCallbacks = new GetCitiesFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			// Чистим текущее поле
			mCitiesIds.clear();
			mCitiesNames.clear();
			
			mCitiesBools.clear();
			mEditOrderTourCity.setText("");
			
			// Ставим нестандартное значение
			mCitiesIds.add("0");
			mCitiesNames.add("Все курорты");
			mCitiesBools.add(false);
		}

		@Override
		public void onProgressUpdate(String... data) {
			mCitiesIds.add(data[0]);
			mCitiesNames.add(data[1]);
			mCitiesBools.add(false);
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				mEditOrderTourCity.setEnabled(true);
			}
		}
	};
	
	private final SendMailFragment.Callbacks mSendMailCallbacks = new SendMailFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
		}

		@Override
		public void onProgressUpdate() {}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
				Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				Toast.makeText(getApplicationContext(), "Заявка на тур принята!\nВ ближайшее время с Вами свяжется менеджер для подтверждения заказа!", Toast.LENGTH_LONG).show();
			}
		}
	};
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_tour);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
	    // Подключаем фрагменты для потоков
	    FragmentManager fm = getFragmentManager();
	    mGetCountriesFragment = (GetCountriesFragment)fm.findFragmentByTag("countries_fragment");
	    mGetCitiesFragment = (GetCitiesFragment)fm.findFragmentByTag("cities_fragment");
	    mSendMailFragment = (SendMailFragment)fm.findFragmentByTag("send_mail_fragment");
	    
	    if (mGetCountriesFragment == null) {
	    	mGetCountriesFragment = GetCountriesFragment.newInstance();
			fm.beginTransaction().add(mGetCountriesFragment, "countries_fragment").commit();
	    }
	    
	    if (mGetCitiesFragment == null) {
	    	mGetCitiesFragment = GetCitiesFragment.newInstance();
			fm.beginTransaction().add(mGetCitiesFragment, "cities_fragment").commit();
	    }
	    
	    if (mSendMailFragment == null) {
	    	mSendMailFragment = SendMailFragment.newInstance();
			fm.beginTransaction().add(mSendMailFragment, "send_mail_fragment").commit();
	    }
	    
	    mGetCountriesFragment.setCallbacks(mGetCountriesCallbacks);
	    mGetCitiesFragment.setCallbacks(mGetCitiesCallbacks);
	    mSendMailFragment.setCallbacks(mSendMailCallbacks);
	    
	    // Инициализируем настройки
	    mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
		
	    // ФИО клиента
	    mEditOrderTourFullName = (EditText)findViewById(R.id.editOrderTourFullName);
	    
	    // Телефон клиента
	    mEditOrderTourContactPhone = (EditText)findViewById(R.id.editOrderTourContactPhone);
	    
		// Город вылета
		mEditOrderTourDepartCity = (EditText)findViewById(R.id.editOrderTourDepartCity);
		mEditOrderTourDepartCity.setOnClickListener(mEditDialogClick);
		 
		JSONArray citiesIds = null;
		JSONArray citiesNames = null;
		
		try {
			citiesIds = new JSONArray(mSettings.getString(MyDevice.APP_PREFS_CITIES_IDS, null));
			citiesNames = new JSONArray(mSettings.getString(MyDevice.APP_PREFS_CITIES_NAMES, null));
		}
		catch (JSONException e) {}
		
		mDepartCitiesIds = new ArrayList<String>();
		mDepartCitiesAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
	    
	    if (citiesIds != null && citiesNames != null) {
	    	for (int i = 0; i < citiesIds.length(); i++){ 
	    		try {
					mDepartCitiesIds.add(citiesIds.getString(i));
					mDepartCitiesAdapter.add(citiesNames.getString(i));
				}
	    		catch (JSONException e) {}
	    	} 
	    }
		
		// Страна
		mEditOrderTourCountry = (EditText)findViewById(R.id.editOrderTourCountry);
		mEditOrderTourCountry.setOnClickListener(mEditDialogClick);
		
		mCountriesIds = new ArrayList<String>();
		mCountriesAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
		
		// Курорт
		mEditOrderTourCity = (EditText)findViewById(R.id.editOrderTourCity);
		mEditOrderTourCity.setOnClickListener(mEditMultiDialogClick);
		
		mCitiesIds = new ArrayList<String>();
		mCitiesNames = new ArrayList<String>();
		mCitiesBools = new ArrayList<Boolean>();
		
	    // Дата вылета с
	    Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
	    mEditOrderTourDateFrom = (EditText)findViewById(R.id.editOrderTourDateFrom);
	    mEditOrderTourDateFrom.setOnClickListener(mEditDatesClick);
	    mEditOrderTourDateFrom.setText(buildDateString(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
	    
	    // Дата вылета по
	    calendar.add(Calendar.DAY_OF_MONTH, 9);
	    mEditOrderTourDateTo = (EditText)findViewById(R.id.editOrderTourDateTo);
	    mEditOrderTourDateTo.setOnClickListener(mEditDatesClick);
	    mEditOrderTourDateTo.setText(buildDateString(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
	    
	    // Ночей с
	    mNightsAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
	 	for (int i = 0; i < 29; i++) {
	 		mNightsAdapter.add((i + 1) + "");
	 	}
	 	
	 	mEditOrderTourNightsFrom = (EditText)findViewById(R.id.editOrderTourNightsFrom);
	 	mEditOrderTourNightsFrom.setOnClickListener(mEditDialogClick);
	 	
	 	// Ночей по
	 	mEditOrderTourNightsTo = (EditText)findViewById(R.id.editOrderTourNightsTo);
	 	mEditOrderTourNightsTo.setOnClickListener(mEditDialogClick);
	 	
	 	// Взрослых
	 	mAdultsAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
	 	mAdultsAdapter.add("1 взрослый");
	 	mAdultsAdapter.add("2 взрослых");
	 	mAdultsAdapter.add("3 взрослых");
	 	mAdultsAdapter.add("4 взрослых");
	 	
	 	mEditOrderTourAdults = (EditText)findViewById(R.id.editOrderTourAdults);
	 	mEditOrderTourAdults.setOnClickListener(mEditDialogClick);
	 	
	 	// Детей
	 	mChildrenAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
	 	mChildrenAdapter.add("Без детей");
	 	mChildrenAdapter.add("1 ребёнок");
	 	mChildrenAdapter.add("2 детей");
	 	mChildrenAdapter.add("3 детей");
	 	
	 	mEditOrderTourChildren = (EditText)findViewById(R.id.editOrderTourChildren);
	 	mEditOrderTourChildren.setOnClickListener(mEditDialogClick);
	 	
	    // Кнопка "Найти"
	    mButtonOrderTourSubmit = (Button)findViewById(R.id.buttonOrderTourSubmit);
	    mButtonOrderTourSubmit.setOnClickListener(mButtonOrderTourSubmitClick);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		NavUtils.navigateUpFromSameTask(this);
	    		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
	
	// Построение строки даты с ведущими нулями
	private String buildDateString(int year, int month, int day) {
		String sDay = Integer.toString(day);
  		String sMonth = Integer.toString(month+1);
		String sYear = Integer.toString(year);
  		
  		if (sDay.length() == 1) sDay = "0" + sDay;
  		if (sMonth.length() == 1) sMonth = "0" + sMonth;
  		
  		return sDay + "." + sMonth + "." + sYear;
	}
	
	// Показ диалога редактирования
	private void showEditDialog(final int id) {
		String title = "";
		int selectedIndex = -1;
		ArrayAdapter<String> adapter = null;
		
		switch (id) {
			case R.id.editOrderTourDepartCity:
				title = "Выберите город вылета";
				adapter = mDepartCitiesAdapter;
				selectedIndex = mDepartCityInd;
				break;
			case R.id.editOrderTourCountry:
				title = "Выберите страну прилёта";
				adapter = mCountriesAdapter;
				selectedIndex = mCountryInd;
				break;
			case R.id.editOrderTourNightsFrom:
				title = "Выберите начальное количество ночей";
				adapter = mNightsAdapter;
				selectedIndex = Integer.parseInt(mEditOrderTourNightsFrom.getText().toString()) - 1;
				break;
			case R.id.editOrderTourNightsTo:
				title = "Выберите конечное количество ночей";
				adapter = mNightsAdapter;
				selectedIndex = Integer.parseInt(mEditOrderTourNightsTo.getText().toString()) - 1;
				break;
			case R.id.editOrderTourAdults:
				title = "Выберите количество взрослых";
				adapter = mAdultsAdapter;
				selectedIndex = mAdultInd;
				break;
			case R.id.editOrderTourChildren:
				title = "Выберите количество детей";
				adapter = mChildrenAdapter;
				selectedIndex = mChildrenInd;
				break;
		}
		
		AlertDialog editDialog = new AlertDialog.Builder(OrderTourActivity.this)
    	.setTitle(title)
    	.setCancelable(true)
    	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int which) {
    			dialog.dismiss();
    		}
    	})
    	.setSingleChoiceItems(adapter, selectedIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	EditText view = null;
            	ArrayAdapter<String> adapter = null;
            	
            	switch (id) {
            		case R.id.editOrderTourDepartCity:
            			view = mEditOrderTourDepartCity;
            			adapter = mDepartCitiesAdapter;
            			
            			mDepartCityInd = which;
            			break;
    				case R.id.editOrderTourCountry:
    					view = mEditOrderTourCountry;
    					adapter = mCountriesAdapter;
    					
    					mCountryInd = which;
    					break;
    				case R.id.editOrderTourNightsFrom:
    					view = mEditOrderTourNightsFrom;
    					adapter = mNightsAdapter;
    					break;
    				case R.id.editOrderTourNightsTo:
    					view = mEditOrderTourNightsTo;
    					adapter = mNightsAdapter;
    					break;
    				case R.id.editOrderTourAdults:
    					view = mEditOrderTourAdults;
    					adapter = mAdultsAdapter;
    					
    					mAdultInd = which;
    					break;
    				case R.id.editOrderTourChildren:
    					view = mEditOrderTourChildren;
    					adapter = mChildrenAdapter;
    					
    					mChildrenInd = which;
    					break;
            	}
            	
            	view.setText(adapter.getItem(which));
            	dialog.dismiss();
            	
            	switch (id) {
        			case R.id.editOrderTourDepartCity:
        				mGetCountriesFragment.execute(mDepartCitiesIds.get(mDepartCityInd));
        				break;
        			case R.id.editOrderTourCountry:
        				mGetCitiesFragment.execute(mCountriesIds.get(mCountryInd));
        				break;
            	}
            }
        })
    	.create();
	
		editDialog.show();
	}
	
	// Показ диалога редактирования с мультивыбором
	private void showEditMultiDialog(final int id) {
		String title = "";
		ArrayList<Boolean> checkedItems = null;
		CharSequence[] adapter = null;
		
		switch (id) {
			case R.id.editOrderTourCity:
				title = "Выберите курорты";
				adapter = mCitiesNames.toArray(new CharSequence[mCitiesNames.size()]);
				checkedItems = mCitiesBools;
				break;
		}
		
		final boolean[] checked = new boolean[checkedItems.size()];
		for (int i = 0; i < checkedItems.size(); i++) {
			checked[i] = checkedItems.get(i);
		}
		
		AlertDialog editMultiDialog = new AlertDialog.Builder(OrderTourActivity.this)
    	.setTitle(title)
    	.setCancelable(true)
    	.setMultiChoiceItems(adapter, checked, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            	if (which == 0) {
            		if (isChecked) {
            			for (int i = 0; i < checked.length; i++) {
            				checked[i] = true;
            				((AlertDialog)dialog).getListView().setItemChecked(i, true);
            			}
            		}
            		else {
            			for (int i = 0; i < checked.length; i++) {
            				checked[i] = false;
            				((AlertDialog)dialog).getListView().setItemChecked(i, false);
            			}
            		}
            	}
            	else {
            		if (!isChecked) {
            			if (checked[0]) {
            				checked[0] = false;
            				((AlertDialog)dialog).getListView().setItemChecked(0, false);
            			}
            		}
            	}
            }
        })
    	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int which) {
    			dialog.dismiss();
    		}
    	})
    	.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int which) {
            	EditText view = null;
            	ArrayList<String> adapter = null;
            	ArrayList<Boolean> checkedItems = null;
            	
            	switch (id) {
            		case R.id.editOrderTourCity:
    					view = mEditOrderTourCity;
    					adapter = mCitiesNames;
    					checkedItems = mCitiesBools;
    					break;
            	}
    			
            	view.setText("");
            	String text = "";
            	String textAll = "";
            	
            	for (int i = 0; i < checked.length; i++) {
            		if (i == 0 && checked[i]) {
            			textAll = adapter.get(i);
            		}
            		
            		checkedItems.set(i, checked[i]);
    				
    				if (checked[i]) {
    					text += adapter.get(i) + ", ";
    				}
    			}
    			
            	if (textAll.length() > 0) {
            		view.setText(textAll);
            	}
            	else if (text.length() >= 2) {
            		view.setText(text.substring(0, text.length() - 2));
            	}
            	else {
            		checked[0] = true;
            		checkedItems.set(0, true);
            		view.setText(view.getHint());
            	}
            	
    			dialog.dismiss();
    		}
    	})
    	.create();
	
		editMultiDialog.show();
	}
}