package ru.ostw.SearchTours;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.app.DialogFragment;

public class DatePickerFragment extends DialogFragment {

	OnDateSetListener onDateSet;
	
	private int year;
	private int month;
	private int day;
	
	public DatePickerFragment() {}
	
	public void setCallBack(OnDateSetListener onDate) {
		onDateSet = onDate;
	}
	
	@Override
	public void setArguments(Bundle args) {
		super.setArguments(args);
		
		year = args.getInt("year");
		month = args.getInt("month");
		day = args.getInt("day");
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new DatePickerDialog(getActivity(), onDateSet, year, month, day);
	}
}