package ru.ostw.SearchTours;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class NewsActivity extends FragmentListActivity {
	
	private ProgressDialog mProgressDialog;
	
	private ParseNewsListFragment mParseNewsListFragment;
	
	private NewsAdapter mListAdapter;
	private ArrayList <HashMap<String, String>> mNewsList;
	
    private static final String KEY_IMAGE = "Image";
    private static final String KEY_HEADER = "Header";
    private static final String KEY_DATE = "Date";
    private static final String KEY_URL = "Url";
    private static final String KEY_SORT = "Sort";
    private static final String KEY_TYPE = "Type";

	private final ParseNewsListFragment.Callbacks mParseNewsListCallbacks = new ParseNewsListFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			mNewsList.clear();
			mListAdapter.notifyDataSetChanged();
		}

		@Override
		public void onProgressUpdate(String... data) {
			HashMap<String, String> map = new HashMap<String, String>();
            
			map.put(KEY_IMAGE, data[0]);
			map.put(KEY_HEADER, data[1]);
			map.put(KEY_DATE, data[2]);
			map.put(KEY_URL, data[3]);
            map.put(KEY_SORT, data[4]);
			map.put(KEY_TYPE, data[5]);
            
            mNewsList.add(map);
            mListAdapter.notifyDataSetChanged();
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
				Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				Collections.sort(mNewsList, new NewsCustomComparator(NewsActivity.KEY_SORT));
				mListAdapter.notifyDataSetChanged();
			}
		}
	};
	
	
	
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Подключаем фрагмент для потоков
	    FragmentManager fm = getFragmentManager();
	    mParseNewsListFragment = (ParseNewsListFragment)fm.findFragmentByTag("parse_news_list_fragment");
	    
	    if (mParseNewsListFragment == null) {
	    	mParseNewsListFragment = ParseNewsListFragment.newInstance();
			fm.beginTransaction().add(mParseNewsListFragment, "parse_news_list_fragment").commit();
	    }
	    
	    mParseNewsListFragment.setCallbacks(mParseNewsListCallbacks);
		
		mNewsList = new ArrayList<HashMap<String, String>>();
		
		mListAdapter = new NewsAdapter(this, R.layout.list_item_news, mNewsList);
		setListAdapter(mListAdapter);
		
		mParseNewsListFragment.execute("http://ost-w.ru");
	}
	
	@Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_news;
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(this, NewsCardActivity.class);
		intent.putExtra("url_id", mNewsList.get(position).get(NewsActivity.KEY_URL));
		intent.putExtra("_id", position);
		
		startActivity(intent);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		NavUtils.navigateUpFromSameTask(this);
	    		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}
