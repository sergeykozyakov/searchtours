package ru.ostw.SearchTours;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
public class MainActivity extends Activity {
	
	private ImageButton mButtonSearchTours;
	private ImageButton mButtonOrderTour;
	private ImageButton mButtonNews;
	private ImageButton mButtonContacts;
	private Button mButtonOrderCallback;
	
	private final OnClickListener mButtonSearchToursClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), SearchToursActivity.class));
		}
	};
	
	private final OnClickListener mButtonOrderTourClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), OrderTourActivity.class));
		}
	};
	
	private final OnClickListener mButtonNewsClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), NewsActivity.class));
		}
	};
	
	private final OnClickListener mButtonContactsClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), ContactsActivity.class));
		}
	};
	
	private final OnClickListener mButtonOrderCallbackClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), OrderCallbackActivity.class));
		}
	};
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mButtonSearchTours = (ImageButton)findViewById(R.id.buttonSearchTours);
		mButtonSearchTours.setOnClickListener(mButtonSearchToursClick);
		
		mButtonOrderTour = (ImageButton)findViewById(R.id.buttonOrderTour);
		mButtonOrderTour.setOnClickListener(mButtonOrderTourClick);
		
		mButtonNews = (ImageButton)findViewById(R.id.buttonNews);
		mButtonNews.setOnClickListener(mButtonNewsClick);
		
		mButtonContacts = (ImageButton)findViewById(R.id.buttonContacts);
		mButtonContacts.setOnClickListener(mButtonContactsClick);
		
		mButtonOrderCallback = (Button)findViewById(R.id.buttonOrderCallback);
		mButtonOrderCallback.setOnClickListener(mButtonOrderCallbackClick);
	}
}