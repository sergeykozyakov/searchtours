package ru.ostw.SearchTours;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

public class ParseNewsListFragment extends Fragment {
	
	private static final String HTTP_ADDR = "http://ost-w.ru";
	
	private Callbacks mCallbacks;
	private HttpTask mTask;
	
	public static interface Callbacks {
		void onPreExecute();
		void onProgressUpdate(String... data);
		void onPostExecute(String error);
	}
	
	public static ParseNewsListFragment newInstance() {
        return new ParseNewsListFragment();
    }
	
	public void setCallbacks(Callbacks callbacks) {
		mCallbacks = callbacks;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mCallbacks = null;
	}
	
	public void execute(String... params) {
		mTask = new HttpTask();
		mTask.execute(params);
	}
	
	public boolean isTaskRunning() {
		if (mTask != null) {
			if (!mTask.isCancelled() && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				return true;
			}
		}
		return false;
	}
	
	private class HttpTask extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (mCallbacks != null) {
				mCallbacks.onPreExecute();
			}
		}
		
		@Override
		protected String doInBackground(String... params) {
			String error;
			
			error = fillData(params[0] + "/novosti/?SHOWALL_1=0", "Новость");
			if (error != null) {
				return error;
			}
			
			error = fillData(params[0] + "/action/", "Акция");
			if (error != null) {
				return error;
			}
			
			return null;
		}
		
	    @Override
	    protected void onProgressUpdate(String... data) {
	    	super.onProgressUpdate(data);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onProgressUpdate(data);
	    	}
	    }
	    
	    @Override
	    protected void onPostExecute(String error) {
	    	super.onPostExecute(error);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onPostExecute(error);
	    	}
	    }
	    
        private String fillData(String url, String type) {
      	  StringBuffer buffer = new StringBuffer();
            try {
                Document doc  = Jsoup.connect(url).get();

                Elements topicList = doc.select("div.action-item");
                buffer.append("action-item list\r\n");
                
                for (Element topic : topicList) {
                    Elements headerNews = topic.getElementsByTag("h2");
                    String headerNewsTopic = headerNews.get(0).text();
                 
                    Elements dateTimeNews = topic.getElementsByTag("span");
                    String dateTimeNewsItem = dateTimeNews.get(0).text();
 
                    Elements hrefNews = topic.getElementsByTag("a");
                    Element hrefNewsElement = hrefNews.get(0);
                    String hr = hrefNewsElement.attr("href");
                    
                    Elements imageNews = topic.getElementsByTag("img");
                    String imgSourceNews = imageNews.get(0).attr("src");
                    
                    String sortKey = "0";

                    if (dateTimeNewsItem.length() == 10) {
                  	  String[] separated = dateTimeNewsItem.split("\\.");
                  	  sortKey = separated[2] + separated[1] + separated[0];
                    }
                    
                    publishProgress(HTTP_ADDR + imgSourceNews, headerNewsTopic, dateTimeNewsItem, hr, sortKey, type);
                }
            }
            catch (Exception e) {
            	return "Невозможно получить новости и акции!";
            }
            catch (Throwable t) {
            	return "Невозможно получить новости и акции!";
            }
            
            return null;
        }
	}
}