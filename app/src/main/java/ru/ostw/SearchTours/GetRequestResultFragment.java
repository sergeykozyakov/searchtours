package ru.ostw.SearchTours;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

public class GetRequestResultFragment extends Fragment {
	
	private static final String SOAP_ACTION = "urn:SletatRu:Contracts:Soap11Gate:v1/Soap11Gate/GetRequestResult";
	private static final String METHOD_NAME = "GetRequestResult";
	private static final String METHOD_NAME_RESULT = "GetRequestResultResult";
	private static final String PROPERTY_REQUEST_ID = "requestId";
	private static final String PROPERTY_ROWS = "Rows";
	private static final String PROPERTY_SOURCE_ID = "SourceId";
	private static final String PROPERTY_OFFER_ID = "OfferId";
	private static final String PROPERTY_COUNTRY_NAME = "CountryName";
	private static final String PROPERTY_RESORT_NAME = "ResortName";
	private static final String PROPERTY_HOTEL_NAME = "HotelName";
	private static final String PROPERTY_STAR_NAME = "StarName";
	private static final String PROPERTY_CHECK_IN_DATE = "CheckInDate";
	private static final String PROPERTY_NIGHTS = "Nights";
	private static final String PROPERTY_PRICE = "Price";
		
	private Callbacks mCallbacks;
	private HttpTask mTask;
	
	public static interface Callbacks {
		void onPreExecute();
		void onProgressUpdate(Tour... data);
		void onPostExecute(String error);
	}
	
	public static GetRequestResultFragment newInstance() {
        return new GetRequestResultFragment();
    }
	
	public void setCallbacks(Callbacks callbacks) {
		mCallbacks = callbacks;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mCallbacks = null;
	}
	
	public void execute(String... params) {
		mTask = new HttpTask();
		mTask.execute(params);
	}
	
	public boolean isTaskRunning() {
		if (mTask != null) {
			if (!mTask.isCancelled() && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				return true;
			}
		}
		return false;
	}
	
	private class HttpTask extends AsyncTask<String, Tour, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (mCallbacks != null) {
				mCallbacks.onPreExecute();
			}
		}
		
		@Override
		protected String doInBackground(String... params) {
			SoapObject request = new SoapObject(SoapAuth.NAMESPACE, METHOD_NAME);
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			
			envelope.headerOut = new Element[1];
			envelope.headerOut[0] = SoapAuth.buildAuthHeader();
			
			request.addProperty(PROPERTY_REQUEST_ID, params[0]);
			
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(SoapAuth.URL);
			
			try {
				androidHttpTransport.call(SOAP_ACTION, envelope);
				
		        SoapObject resultsRequestSOAP = (SoapObject)envelope.bodyIn;
				SoapObject result = (SoapObject)resultsRequestSOAP.getProperty(METHOD_NAME_RESULT);
				
				SoapObject resultRows = (SoapObject)result.getProperty(PROPERTY_ROWS);
				
				for (int i = 0; i < resultRows.getPropertyCount(); i++) {
					if (i > 99) break;
					SoapObject resultRow = (SoapObject)resultRows.getProperty(i);
					
					publishProgress(new Tour(Integer.parseInt(resultRow.getProperty(PROPERTY_SOURCE_ID).toString()), Integer.parseInt(resultRow.getProperty(PROPERTY_OFFER_ID).toString()),
						resultRow.getProperty(PROPERTY_COUNTRY_NAME).toString(), resultRow.getProperty(PROPERTY_RESORT_NAME).toString(),
						resultRow.getProperty(PROPERTY_HOTEL_NAME).toString(), resultRow.getProperty(PROPERTY_STAR_NAME).toString(),
						resultRow.getProperty(PROPERTY_CHECK_IN_DATE).toString(), resultRow.getProperty(PROPERTY_NIGHTS).toString(),
						resultRow.getProperty(PROPERTY_PRICE).toString()));
				}
			}
			catch (Exception e) {
				return "Не удалось получить результаты поиска туров!";
			}
	        
			return null;
		}
		
	    @Override
	    protected void onProgressUpdate(Tour... data) {
	    	super.onProgressUpdate(data);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onProgressUpdate(data);
	    	}
	    }
	    
	    @Override
	    protected void onPostExecute(String error) {
	    	super.onPostExecute(error);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onPostExecute(error);
	    	}
	    }
	}
}