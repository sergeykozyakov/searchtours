package ru.ostw.SearchTours;

import java.util.Comparator;
import java.util.Map;

// Переопределяем сравнение
public class NewsCustomComparator implements Comparator<Map<String, String>> {
    private final String key;

    public NewsCustomComparator(String key) {
        this.key = key;     
    }

    // Сравниваем
    public int compare(Map<String, String> first, Map<String, String> second) {
        String firstValue = first.get(key);
        String secondValue = second.get(key);

        return secondValue.compareTo(firstValue);
    }
}