package ru.ostw.SearchTours;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TourCardActivity extends FragmentActivity {
	
	private ProgressDialog mProgressDialog;
	
	private SendMailFragment mSendMailFragment;
	
	private String[] mTourData;
	
	private TextView mTextTourCardResortName;
	private TextView mTextTourCardHotelName;
	private TextView mTextTourCardStarName;
	private TextView mTextTourCardPrice;
	private TextView mTextTourCardDepartCity;
	private TextView mTextTourCardCheckInDate;
	private TextView mTextTourCardNights;
	private TextView mTextTourCardRoomName;
	private TextView mTextTourCardHtPlaceName;
	private TextView mTextTourCardMealName;
	
	private ImageView mImageTourCardStar1;
	private ImageView mImageTourCardStar2;
	private ImageView mImageTourCardStar3;
	private ImageView mImageTourCardStar4;
	private ImageView mImageTourCardStar5;
	
	private Button mButtonTourCardOrder;

	// Слушатель нажатия на кнопку "Купить тур"
	private final OnClickListener mButtonTourCardOrderClick = new OnClickListener() {
		@SuppressLint("InflateParams")
		@Override
		public void onClick(View v) {
			LinearLayout view = (LinearLayout)getLayoutInflater().inflate(R.layout.edit_dialog_contact, null);
			final EditText fullName = (EditText)view.findViewById(R.id.editDialogContactFullName);
			final EditText contactPhone = (EditText)view.findViewById(R.id.editDialogContactPhone);
			
			AlertDialog contactDialog = new AlertDialog.Builder(TourCardActivity.this)
			.setTitle("Ваши контактные данные")
	    	.setCancelable(true)
	    	.setView(view)
	    	.setPositiveButton("Купить тур", new DialogInterface.OnClickListener() {
	    		public void onClick(DialogInterface dialog, int id) {
	    			if (fullName.length() == 0 || contactPhone.length() == 0) {
	    				Toast.makeText(getApplicationContext(), "Заполните все поля!", Toast.LENGTH_SHORT).show();
	    				return;
	    			}
	    			
	    			// Отправка запроса по почте
	    			mSendMailFragment.execute("Новая заявка на тур из мобильного приложения",   
	    				"Поступила заявка на покупку тура из мобильного приложения Android.\n\n" +
	    				"ФИО: " + fullName.getText().toString() + "\n" +
	    				"Контактный телефон: " + contactPhone.getText().toString() + "\n\n" +
	    				"Город вылета: " + mTextTourCardDepartCity.getText().toString() + "\n\n" +
	    				"Страна прилёта: " + mTourData[1] + "\n" +
	    				"Курорт: " + mTourData[2] + "\n" +
	    				"Отель: " + mTextTourCardHotelName.getText().toString() + " (" + mTourData[4] + ")\n\n" +
	    				"Дата вылета: " + mTextTourCardCheckInDate.getText().toString() + "\n" +
	    				"Ночей: " + mTextTourCardNights.getText().toString() + "\n\n" +
	    				"Тип номера: " + mTextTourCardRoomName.getText().toString() + "\n" +
	    				"Размещение: " + mTextTourCardHtPlaceName.getText().toString() + "\n" +
	    				"Питание: " + mTextTourCardMealName.getText().toString() + "\n\n" +
	    				"Стоимость тура: " + mTextTourCardPrice.getText().toString());
	    			
	    			dialog.dismiss();
	        	}
	    	})
	    	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
	    		public void onClick(DialogInterface dialog, int id) {
	    			dialog.dismiss();
	    		}
	    	})
	    	.create();
		
			contactDialog.show();
		}
	};
	
	private final SendMailFragment.Callbacks mSendMailCallbacks = new SendMailFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
		}

		@Override
		public void onProgressUpdate() {}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
				Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				Toast.makeText(getApplicationContext(), "Заявка на тур принята!\nВ ближайшее время с Вами свяжется менеджер для подтверждения заказа!", Toast.LENGTH_LONG).show();
			}
		}
	};
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tour_card);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Подключаем фрагмент для потоков
	    FragmentManager fm = getFragmentManager();
	    mSendMailFragment = (SendMailFragment)fm.findFragmentByTag("send_mail_fragment");
	    
	    if (mSendMailFragment == null) {
	    	mSendMailFragment = SendMailFragment.newInstance();
			fm.beginTransaction().add(mSendMailFragment, "send_mail_fragment").commit();
	    }
	    
	    mSendMailFragment.setCallbacks(mSendMailCallbacks);
		
		Intent intent = getIntent();
    	mTourData = intent.getStringArrayExtra("tour_data");
    	
    	if (mTourData == null) {
    		Toast.makeText(this, "Невозможно отобразить расширенную информацию о туре.", Toast.LENGTH_SHORT).show();
    		finish();
    		return;
    	}
    	    	
    	mTextTourCardResortName = (TextView)findViewById(R.id.textTourCardResortName);
    	mTextTourCardHotelName = (TextView)findViewById(R.id.textTourCardHotelName);
    	mTextTourCardStarName = (TextView)findViewById(R.id.textTourCardStarName);
    	mTextTourCardPrice = (TextView)findViewById(R.id.textTourCardPrice);
    	mTextTourCardDepartCity = (TextView)findViewById(R.id.textTourCardDepartCity);
    	mTextTourCardCheckInDate = (TextView)findViewById(R.id.textTourCardCheckInDate);
    	mTextTourCardNights = (TextView)findViewById(R.id.textTourCardNights);
    	mTextTourCardRoomName = (TextView)findViewById(R.id.textTourCardRoomName);
    	mTextTourCardHtPlaceName = (TextView)findViewById(R.id.textTourCardHtPlaceName);
    	mTextTourCardMealName = (TextView)findViewById(R.id.textTourCardMealName);
    	
    	mImageTourCardStar1 = (ImageView)findViewById(R.id.imageTourCardStar1);
    	mImageTourCardStar2 = (ImageView)findViewById(R.id.imageTourCardStar2);
    	mImageTourCardStar3 = (ImageView)findViewById(R.id.imageTourCardStar3);
    	mImageTourCardStar4 = (ImageView)findViewById(R.id.imageTourCardStar4);
    	mImageTourCardStar5 = (ImageView)findViewById(R.id.imageTourCardStar5);
    	
    	mButtonTourCardOrder = (Button)findViewById(R.id.buttonTourCardOrder);
    	mButtonTourCardOrder.setOnClickListener(mButtonTourCardOrderClick);
    	
    	mTextTourCardResortName.setText(mTourData[1] + ", " + mTourData[2]);
    	mTextTourCardHotelName.setText(mTourData[3]);
    	mTextTourCardPrice.setText(mTourData[9] + " руб.");
    	mTextTourCardDepartCity.setText(mTourData[0]);
    	mTextTourCardCheckInDate.setText(mTourData[6]);
    	mTextTourCardNights.setText(mTourData[5]);
    	mTextTourCardRoomName.setText(mTourData[7]);
    	mTextTourCardHtPlaceName.setText(mTourData[10]);
    	mTextTourCardMealName.setText(mTourData[8]);
    	
		if (mTourData[4].contains("1")) {
			mImageTourCardStar1.setVisibility(View.VISIBLE);
		}
		else if (mTourData[4].contains("2")) {
			mImageTourCardStar1.setVisibility(View.VISIBLE);
			mImageTourCardStar2.setVisibility(View.VISIBLE);
		}
		else if (mTourData[4].contains("3")) {
			mImageTourCardStar1.setVisibility(View.VISIBLE);
			mImageTourCardStar2.setVisibility(View.VISIBLE);
			mImageTourCardStar3.setVisibility(View.VISIBLE);
		}
		else if (mTourData[4].contains("4")) {
			mImageTourCardStar1.setVisibility(View.VISIBLE);
			mImageTourCardStar2.setVisibility(View.VISIBLE);
			mImageTourCardStar3.setVisibility(View.VISIBLE);
			mImageTourCardStar4.setVisibility(View.VISIBLE);
		}
		else if (mTourData[4].contains("5")) {
			mImageTourCardStar1.setVisibility(View.VISIBLE);
			mImageTourCardStar2.setVisibility(View.VISIBLE);
			mImageTourCardStar3.setVisibility(View.VISIBLE);
			mImageTourCardStar4.setVisibility(View.VISIBLE);
			mImageTourCardStar5.setVisibility(View.VISIBLE);
		}
		else {
			mTextTourCardStarName.setText(mTourData[4]);
			mTextTourCardStarName.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		onBackPressed();
	    		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}