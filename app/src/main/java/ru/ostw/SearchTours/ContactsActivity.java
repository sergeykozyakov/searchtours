package ru.ostw.SearchTours;

import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

public class ContactsActivity extends FragmentListActivity implements OnMapReadyCallback {
	
	private MapFragment mMapFragment;
	private ContactsAdapter mListAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacts);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		TabHost tabs = (TabHost)findViewById(android.R.id.tabhost);
		tabs.setup();

		TabHost.TabSpec spec = tabs.newTabSpec("tag1");

		spec.setContent(R.id.tabContacts1);
		spec.setIndicator("Список");
		tabs.addTab(spec);

		spec = tabs.newTabSpec("tag2");
		spec.setContent(R.id.tabContacts2);
		spec.setIndicator("Карта");
		tabs.addTab(spec);

		tabs.setCurrentTab(0);
		
		mListAdapter = new ContactsAdapter(this, R.layout.list_item_contact, new ArrayList<Contact>());
	    setListAdapter(mListAdapter);
		
	    mListAdapter.add(new Contact(new LatLng(56.836057, 60.609461), "Головной офис", "г. Екатеринбург, ул.Пушкина, 4\n+7 (343) 206-62-62", "+73432066262"));
	    mListAdapter.add(new Contact(new LatLng(56.856127, 60.639460), "Офис продаж", "г. Екатеринбург, ул. Советская, 41\n+7 (343) 206-16-16", "+73432061616"));
	    mListAdapter.add(new Contact(new LatLng(56.838531, 60.609010), "Офис продаж", "г. Екатеринбург, пр. Ленина, 36 (напротив к/т «Салют»)\n+7 (343) 206-35-35", "+73432063535"));
	    mListAdapter.add(new Contact(new LatLng(56.792228, 60.626718), "Офис продаж", "г. Екатеринбург, ул. Родонитовая, 22\n+7 (343) 206-37-37", "+73432063737"));
	    mListAdapter.add(new Contact(new LatLng(56.817038, 60.603655), "Офис продаж", "г. Екатеринбург, ул. Большакова, 99а\n+7 (343) 345-54-00", "+73433455400"));
	    mListAdapter.add(new Contact(new LatLng(56.888716, 60.614368), "Офис продаж", "г. Екатеринбург, пр. Космонавтов, 46\n+7 (343) 207-35-35", "+73432073535"));
	    mListAdapter.add(new Contact(new LatLng(56.834779, 60.686278), "Офис продаж", "г. Екатеринбург, ул. Сыромолотова, 24\n+7 (343)207-22-27", "+73432072227"));
	    mListAdapter.add(new Contact(new LatLng(56.903974, 60.598298), "Офис продаж", "г. Екатеринбург, ул. 40 лет Октября, 75, ТЦ «Калинка» (2 этаж)\n+7 (343) 344–80–84", "+73433448084"));
	    mListAdapter.add(new Contact(new LatLng(56.797556, 60.581825), "Офис продаж", "г. Екатеринбург, ул. Амундсена, 63, Экомолл «Гранат»\n+7 (343) 345-54-00", "+73433455400"));
	    mListAdapter.add(new Contact(new LatLng(56.807211, 60.611694), "Офис продаж", "г. Екатеринбург, ул. 8 Марта, 149, ТРЦ «Мегаполис» (1 этаж)\n+7 (343) 381-08-05", "+73433810805"));
	    mListAdapter.add(new Contact(new LatLng(56.791257, 60.516829), "Офис продаж", "г. Екатеринбург, ул. Вильгельма де Геннина, 40\n+7 (343) 345-54-00", "+73433455400"));
	    mListAdapter.add(new Contact(new LatLng(56.778908, 60.548936), "Офис продаж", "г. Екатеринбург, ул. Краснолесья, 12\n+7 (343) 214-80-01", "+73432148001"));
	    
		// Подключаем карту
		FragmentManager fm = getFragmentManager();
        mMapFragment = (MapFragment)fm.findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
	}

	@Override
	public void onMapReady(GoogleMap map) {
        if (map != null) {
            map.setMyLocationEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);

            map.setInfoWindowAdapter(new InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @SuppressLint("InflateParams")
                @Override
                public View getInfoContents(Marker marker) {
                    View v = getLayoutInflater().inflate(R.layout.layout_marker_info, null);
                    TextView title = (TextView)v.findViewById(R.id.textMarkerTitle);
                    TextView snippet = (TextView)v.findViewById(R.id.textMarkerSnippet);
                    title.setText(marker.getTitle());
                    snippet.setText(marker.getSnippet());

                    return v;
                }
            });

            map.clear();

            for (int i = 0; i < mListAdapter.getCount(); i++) {
                map.addMarker(new MarkerOptions()
                        .position(mListAdapter.getItem(i).location)
                        .title(mListAdapter.getItem(i).title)
                        .snippet(mListAdapter.getItem(i).snippet));
            }

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(56.846057, 60.609461), 11));
        }
        else {
            Toast.makeText(this, "Невозможно отобразить карту на вашем устройстве!\nВоспользуйтесь списком офисов.", Toast.LENGTH_LONG).show();
        }
	}
	
	@Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_contacts;
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
		final String tel = ((Contact)l.getAdapter().getItem(position)).tel;
		
		AlertDialog callDialog = new AlertDialog.Builder(ContactsActivity.this)
    	.setMessage("Набрать номер выбранного офиса?")
    	.setCancelable(true)
    	.setPositiveButton("Да", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int id) {
            	dialog.dismiss();
            	
        		if (!MyDevice.isTelephonyEnabled(getApplicationContext())) {
        			Toast.makeText(getApplicationContext(), "Невозможно набрать телефонный номер на вашем устройстве!", Toast.LENGTH_LONG).show();
        			return;
        		}
        		
        		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tel));
        		startActivity(intent);
        	}
    	})
    	.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int id) {
    			dialog.dismiss();
    		}
    	})
    	.create();
	
		callDialog.show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		NavUtils.navigateUpFromSameTask(this);
	    		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
