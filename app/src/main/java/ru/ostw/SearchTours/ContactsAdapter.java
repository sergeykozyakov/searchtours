package ru.ostw.SearchTours;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ContactsAdapter extends ArrayAdapter<Contact> {
	private Context context; 
	private int layoutResourceId;
	private ArrayList<Contact> data = null;

	public ContactsAdapter(Context context, int layoutResourceId, ArrayList<Contact> data) {
		super(context, layoutResourceId, data);
		
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		View row = convertView;
		ContactsHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new ContactsHolder();
			holder.textContactTitle = (TextView)row.findViewById(R.id.textContactTitle);
			holder.textContactSnippet = (TextView)row.findViewById(R.id.textContactSnippet);

			row.setTag(holder);
		}
		else {
			holder = (ContactsHolder)row.getTag();
		}

		Contact contact = data.get(position);
		
		holder.textContactTitle.setText(contact.title);
		holder.textContactSnippet.setText(contact.snippet);

		return row;
	}

	private static class ContactsHolder {
		TextView textContactTitle;
		TextView textContactSnippet;
	}
}
