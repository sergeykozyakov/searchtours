package ru.ostw.SearchTours;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

public class NewsCardActivity extends FragmentActivity {
	
	private ProgressDialog mProgressDialog;
	
	private ParseNewsFragment mParseNewsFragment;
	
	private WebView mWebView;
	private static final String HTTP_ADDR = "http://www.ost-w.ru";
	
	private final ParseNewsFragment.Callbacks mParseNewsCallbacks = new ParseNewsFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
		}

		@Override
		public void onProgressUpdate(String... data) {
			mWebView.loadDataWithBaseURL(HTTP_ADDR,  data[0], "text/html", "UTF-8", "");
            setTitle(data[1]);
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
				Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
		}
	};
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_card);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Подключаем фрагмент для потоков
	    FragmentManager fm = getFragmentManager();
	    mParseNewsFragment = (ParseNewsFragment)fm.findFragmentByTag("parse_news_fragment");
	    
	    if (mParseNewsFragment == null) {
	    	mParseNewsFragment = ParseNewsFragment.newInstance();
			fm.beginTransaction().add(mParseNewsFragment, "parse_news_fragment").commit();
	    }
	    
	    mParseNewsFragment.setCallbacks(mParseNewsCallbacks);
		
		String url = getIntent().getStringExtra("url_id");
        int id = getIntent().getIntExtra("_id", -1);
        
        if (url == null || id == -1) {
        	Toast.makeText(this, "Не удалось получить текст новости!", Toast.LENGTH_SHORT).show();
        	finish();
        	
        	return;
        }
        
        mWebView = (WebView)findViewById(R.id.webViewNews);
        mWebView.getSettings().setJavaScriptEnabled(false);
        mWebView.getSettings().setBlockNetworkImage(false);
        mWebView.getSettings().setDefaultTextEncodingName("windows-1251");
        
        int width = (int)Math.ceil(mWebView.getWidth()*0.55);
        mParseNewsFragment.execute(url, Integer.toString(width));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		onBackPressed();
	    		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}