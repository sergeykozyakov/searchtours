package ru.ostw.SearchTours;

import com.google.android.gms.maps.model.LatLng;

public class Contact {
	
	public LatLng location;
	public String title;
	public String snippet;
	public String tel;
	
	public Contact(LatLng location, String title, String snippet, String tel) {
		
		this.location = location;
		this.title = title;
		this.snippet = snippet;
		this.tel = tel;
	}
}