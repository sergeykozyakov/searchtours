package ru.ostw.SearchTours;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class OrderCallbackActivity extends FragmentActivity {
	
	private ProgressDialog mProgressDialog;
	
	private SendMailFragment mSendMailFragment;
	
	private EditText mEditOrderCallbackFullName;
	private EditText mEditOrderCallbackContactPhone;
	private Button mButtonOrderCallbackSubmit;
	
	// Слушатель нажатия на кнопку "Перезвонить"
	private final OnClickListener mButtonOrderCallbackSubmitClick = new OnClickListener() {
		public void onClick(View v) {
			if (mEditOrderCallbackFullName.length() == 0 || mEditOrderCallbackContactPhone.length() == 0) {
				Toast.makeText(getApplicationContext(), "Укажите свои контактные данные!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Отправка запроса по почте
			mSendMailFragment.execute("Запрос обратного звонка из мобильного приложения",   
				"Поступил запрос на обратный звонок из мобильного приложения Android.\n\n" +
			    "ФИО: " + mEditOrderCallbackFullName.getText().toString() + "\n" +
			    "Контактный телефон: " + mEditOrderCallbackContactPhone.getText().toString());
		}
	};
	
	private final SendMailFragment.Callbacks mSendMailCallbacks = new SendMailFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
		}

		@Override
		public void onProgressUpdate() {}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
				Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				Toast.makeText(getApplicationContext(), "Обратный звонок заказан!\nВ ближайшее время с Вами свяжется менеджер!", Toast.LENGTH_LONG).show();
			}
		}
	};



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_callback);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Подключаем фрагмент для потоков
	    FragmentManager fm = getFragmentManager();
	    mSendMailFragment = (SendMailFragment)fm.findFragmentByTag("send_mail_fragment");
	    
	    if (mSendMailFragment == null) {
	    	mSendMailFragment = SendMailFragment.newInstance();
			fm.beginTransaction().add(mSendMailFragment, "send_mail_fragment").commit();
	    }
	    
	    mSendMailFragment.setCallbacks(mSendMailCallbacks);
	    
	    mEditOrderCallbackFullName = (EditText)findViewById(R.id.editOrderCallbackFullName);
		mEditOrderCallbackContactPhone = (EditText)findViewById(R.id.editOrderCallbackContactPhone);
		
		mButtonOrderCallbackSubmit = (Button)findViewById(R.id.buttonOrderCallbackSubmit);
		mButtonOrderCallbackSubmit.setOnClickListener(mButtonOrderCallbackSubmitClick);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		NavUtils.navigateUpFromSameTask(this);
	    		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}