package ru.ostw.SearchTours;

import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

public class CreateRequestFragment extends Fragment {
	
	private static final String SOAP_ACTION = "urn:SletatRu:Contracts:Soap11Gate:v1/Soap11Gate/CreateRequest";
	private static final String METHOD_NAME = "CreateRequest";
	private static final String METHOD_NAME_RESULT = "CreateRequestResult";
	private static final String PROPERTY_COUNTRY_ID = "countryId";
	private static final String PROPERTY_CITY_FROM_ID = "cityFromId";
	private static final String PROPERTY_CITIES = "cities";
	private static final String PROPERTY_MEALS = "meals";
	private static final String PROPERTY_STARS = "stars";
	private static final String PROPERTY_HOTELS = "hotels";
	private static final String PROPERTY_ADULTS = "adults";
	private static final String PROPERTY_KIDS = "kids";
	private static final String PROPERTY_KIDS_AGES = "kidsAges";
	private static final String PROPERTY_NIGHTS_MIN = "nightsMin";
	private static final String PROPERTY_NIGHTS_MAX = "nightsMax";
	private static final String PROPERTY_CURRENCY_ALIAS = "currencyAlias";
	private static final String PROPERTY_DEPART_FROM = "departFrom";
	private static final String PROPERTY_DEPART_TO = "departTo";
	private static final String PROPERTY_HOTEL_IS_NOT_IN_STOP = "hotelIsNotInStop";
	private static final String PROPERTY_HAS_TICKETS = "hasTickets";
	private static final String PROPERTY_TICKETS_INCLUDED = "ticketsIncluded";
	private static final String PROPERTY_USE_FILTER = "useFilter";
	private static final String PROPERTY_F_TO_ID = "f_to_id";
	private static final String PROPERTY_INCLUDE_DESCRIPTIONS = "includeDescriptions";
	
	private Callbacks mCallbacks;
	private HttpTask mTask;
	
	public static interface Callbacks {
		void onPreExecute();
		void onProgressUpdate(String... data);
		void onPostExecute(String error);
	}
	
	public static CreateRequestFragment newInstance() {
        return new CreateRequestFragment();
    }
	
	public void setCallbacks(Callbacks callbacks) {
		mCallbacks = callbacks;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mCallbacks = null;
	}
	
	public void execute(Object... params) {
		mTask = new HttpTask();
		mTask.execute(params);
	}
	
	public boolean isTaskRunning() {
		if (mTask != null) {
			if (!mTask.isCancelled() && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				return true;
			}
		}
		return false;
	}
	
	private class HttpTask extends AsyncTask<Object, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (mCallbacks != null) {
				mCallbacks.onPreExecute();
			}
		}
		
		@SuppressWarnings("unchecked")
		@Override
		protected String doInBackground(Object... params) {
			SoapObject request = new SoapObject(SoapAuth.NAMESPACE, METHOD_NAME);
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			
			envelope.dotNet = true;
			envelope.implicitTypes = true;
			envelope.encodingStyle = SoapSerializationEnvelope.XSD;
			
			MarshalArrayOfInt marArrInt = new MarshalArrayOfInt();
			marArrInt.register(envelope);
			
			envelope.headerOut = new Element[1];
			envelope.headerOut[0] = SoapAuth.buildAuthHeader();
			
			ArrayList<Integer> arrayCities = (ArrayList<Integer>)params[2];
			int[] soapCities = new int[arrayCities.size()];
			
			for (int i = 0; i < arrayCities.size(); i++) {
				soapCities[i] = arrayCities.get(i);
			}
			
			ArrayList<Integer> arrayStars = (ArrayList<Integer>)params[3];
			int[] soapStars = new int[arrayStars.size()];
			
			for (int i = 0; i < arrayStars.size(); i++) {
				soapStars[i] = arrayStars.get(i);
			}
			
			ArrayList<Integer> arrayHotels = (ArrayList<Integer>)params[4];
			int[] soapHotels = new int[arrayHotels.size()];
			
			for (int i = 0; i < arrayHotels.size(); i++) {
				soapHotels[i] = arrayHotels.get(i);
			}
			
			request.addProperty(PROPERTY_COUNTRY_ID, (String)params[0]);
			request.addProperty(PROPERTY_CITY_FROM_ID, (String)params[1]);
			request.addProperty(PROPERTY_CITIES, soapCities);
			request.addProperty(PROPERTY_MEALS, new int[0]);
			request.addProperty(PROPERTY_STARS, soapStars);
			request.addProperty(PROPERTY_HOTELS, soapHotels);
			request.addProperty(PROPERTY_ADULTS, (String)params[5]);
			request.addProperty(PROPERTY_KIDS, (String)params[6]);
			request.addProperty(PROPERTY_KIDS_AGES, new int[0]);
			request.addProperty(PROPERTY_NIGHTS_MIN, (String)params[7]);
			request.addProperty(PROPERTY_NIGHTS_MAX, (String)params[8]);
			request.addProperty(PROPERTY_CURRENCY_ALIAS, "RUB");
			request.addProperty(PROPERTY_DEPART_FROM, (String)params[9]);
			request.addProperty(PROPERTY_DEPART_TO, (String)params[10]);
			request.addProperty(PROPERTY_HOTEL_IS_NOT_IN_STOP, "true");
			request.addProperty(PROPERTY_HAS_TICKETS, "true");
			request.addProperty(PROPERTY_TICKETS_INCLUDED, "true");
			request.addProperty(PROPERTY_USE_FILTER, "false");
			request.addProperty(PROPERTY_F_TO_ID, new int[0]);
			request.addProperty(PROPERTY_INCLUDE_DESCRIPTIONS, "true");
			
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(SoapAuth.URL);
			
			try {
				androidHttpTransport.call(SOAP_ACTION, envelope);
				
				SoapObject resultsRequestSOAP = (SoapObject)envelope.bodyIn;
				String result = (String)resultsRequestSOAP.getProperty(METHOD_NAME_RESULT).toString();
				
				publishProgress(result);
			}
			catch (Exception e) {
				return "Не удалось отправить запрос на поиск тура!";
			}
	        
			return null;
		}
		
	    @Override
	    protected void onProgressUpdate(String... data) {
	    	super.onProgressUpdate(data);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onProgressUpdate(data);
	    	}
	    }
	    
	    @Override
	    protected void onPostExecute(String error) {
	    	super.onPostExecute(error);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onPostExecute(error);
	    	}
	    }
	}
}