package ru.ostw.SearchTours;

import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

public class GetHotelsFragment extends Fragment {
	
	private static final String SOAP_ACTION = "urn:SletatRu:Contracts:Soap11Gate:v1/Soap11Gate/GetHotels";
	private static final String METHOD_NAME = "GetHotels";
	private static final String METHOD_NAME_RESULT = "GetHotelsResult";
	private static final String PROPERTY_COUNTRY_ID = "countryId";
	private static final String PROPERTY_TOWNS = "towns";
	private static final String PROPERTY_STARS = "stars";
	private static final String PROPERTY_FILTER = "filter";
	private static final String PROPERTY_COUNT = "count";
	private static final String PROPERTY_ID = "Id";
	private static final String PROPERTY_NAME = "Name";
	
	private Callbacks mCallbacks;
	private HttpTask mTask;
	
	public static interface Callbacks {
		void onPreExecute();
		void onProgressUpdate(String... data);
		void onPostExecute(String error);
	}
	
	public static GetHotelsFragment newInstance() {
        return new GetHotelsFragment();
    }
	
	public void setCallbacks(Callbacks callbacks) {
		mCallbacks = callbacks;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mCallbacks = null;
	}
	
	public void execute(Object... params) {
		mTask = new HttpTask();
		mTask.execute(params);
	}
	
	public boolean isTaskRunning() {
		if (mTask != null) {
			if (!mTask.isCancelled() && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				return true;
			}
		}
		return false;
	}
	
	private class HttpTask extends AsyncTask<Object, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (mCallbacks != null) {
				mCallbacks.onPreExecute();
			}
		}
		
		@SuppressWarnings("unchecked")
		@Override
		protected String doInBackground(Object... params) {
			SoapObject request = new SoapObject(SoapAuth.NAMESPACE, METHOD_NAME);
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			
			envelope.dotNet = true;
			envelope.implicitTypes = true;
			envelope.encodingStyle = SoapSerializationEnvelope.XSD;
			
			MarshalArrayOfInt marArrInt = new MarshalArrayOfInt();
			marArrInt.register(envelope);
			
			envelope.headerOut = new Element[1];
			envelope.headerOut[0] = SoapAuth.buildAuthHeader();
			
			ArrayList<Integer> arrayTowns = (ArrayList<Integer>)params[1];
			int[] soapTowns = new int[arrayTowns.size()];
			
			for (int i = 0; i < arrayTowns.size(); i++) {
				soapTowns[i] = arrayTowns.get(i);
			}
			
			ArrayList<Integer> arrayStars = (ArrayList<Integer>)params[2];
			int[] soapStars = new int[arrayStars.size()];
			
			for (int i = 0; i < arrayStars.size(); i++) {
				soapStars[i] = arrayStars.get(i);
			}
			
			request.addProperty(PROPERTY_COUNTRY_ID, (String)params[0]);
			request.addProperty(PROPERTY_TOWNS, soapTowns);
			request.addProperty(PROPERTY_STARS, soapStars);
			request.addProperty(PROPERTY_FILTER, "");
			request.addProperty(PROPERTY_COUNT, "200");
			
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(SoapAuth.URL);
			
			try {
				androidHttpTransport.call(SOAP_ACTION, envelope);
				
		        SoapObject resultsRequestSOAP = (SoapObject)envelope.bodyIn;
				SoapObject result = (SoapObject)resultsRequestSOAP.getProperty(METHOD_NAME_RESULT);
				
				for (int i = 0; i < result.getPropertyCount(); i++) {
					SoapObject resultRow = (SoapObject)result.getProperty(i);
					publishProgress(resultRow.getProperty(PROPERTY_ID).toString(), resultRow.getProperty(PROPERTY_NAME).toString());
				}
			}
			catch (Exception e) {
				return "Не удалось получить список отелей!";
			}
	        
			return null;
		}
		
	    @Override
	    protected void onProgressUpdate(String... data) {
	    	super.onProgressUpdate(data);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onProgressUpdate(data);
	    	}
	    }
	    
	    @Override
	    protected void onPostExecute(String error) {
	    	super.onPostExecute(error);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onPostExecute(error);
	    	}
	    }
	}
}