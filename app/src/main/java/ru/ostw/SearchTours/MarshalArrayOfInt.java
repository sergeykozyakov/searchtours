package ru.ostw.SearchTours;

import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

public class MarshalArrayOfInt implements Marshal {
	
    public MarshalArrayOfInt() {}

    @Override
    public Object readInstance(XmlPullParser parser, String namespace, String name, PropertyInfo expected) throws IOException, XmlPullParserException {
        return parser.nextText();
    }

    @Override
    public void writeInstance(XmlSerializer writer, Object obj) throws IOException {
        final String NAMESPACE = "http://schemas.microsoft.com/2003/10/Serialization/Arrays";
        int[] arr = (int[])obj;
        
        for (int i : arr) {
            writer.startTag(NAMESPACE, "int");
            writer.text(Integer.toString(i));
            writer.endTag(NAMESPACE, "int");
        }
    }

    @Override
    public void register(SoapSerializationEnvelope sse) {
        sse.addMapping(sse.xsd, "ArrayOfint", int[].class, this);
    }
}