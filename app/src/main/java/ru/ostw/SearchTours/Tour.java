package ru.ostw.SearchTours;

public class Tour {
	
	public int sourceId;
	public int offerId;
	
	public String countryName;
	public String resortName;
	public String hotelName;
	public String starName;
	public String checkInDate;
	public String nights;
	public String price;
	
	public Tour(int sourceId, int offerId, String countryName, String resortName, String hotelName, String starName,
		String checkInDate, String nights, String price) {
		
		this.sourceId = sourceId;
		this.offerId = offerId;
		
		this.countryName = countryName;
		this.resortName = resortName;
		this.hotelName = hotelName;
		this.starName = starName;
		this.checkInDate = checkInDate;
		this.nights = nights;
		this.price = price;
	}
}