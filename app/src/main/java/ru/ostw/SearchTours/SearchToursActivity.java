package ru.ostw.SearchTours;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class SearchToursActivity extends FragmentActivity {
	
	private SharedPreferences mSettings;
	private ProgressDialog mProgressDialog;
	private ProgressDialog mProgressDialogHorizontal;
	
	private GetCountriesFragment mGetCountriesFragment;
	private GetCitiesFragment mGetCitiesFragment;
	private GetHotelStarsFragment mGetHotelStarsFragment;
	private GetHotelsFragment mGetHotelsFragment;
	private CreateRequestFragment mCreateRequestFragment;
	private GetRequestStateFragment mGetRequestStateFragment;
	
	private int mRequestId = 0;
	private int mNumIterations = 20;
	private ArrayList<Boolean> mIsProcessedList;
		
	private ArrayList<String> mDepartCitiesIds;
	private ArrayList<String> mCountriesIds;
	private ArrayList<String> mCitiesIds;
	private ArrayList<String> mHotelStarsIds;
	private ArrayList<String> mHotelsIds;
	
	private ArrayList<String> mCitiesNames;
	private ArrayList<String> mHotelStarsNames;
	private ArrayList<String> mHotelsNames;
	
	private ArrayList<Boolean> mCitiesBools;
	private ArrayList<Boolean> mHotelStarsBools;
	private ArrayList<Boolean> mHotelsBools;
	
	private ArrayAdapter<String> mDepartCitiesAdapter;
	private ArrayAdapter<String> mCountriesAdapter;
	private ArrayAdapter<String> mNightsAdapter;
	private ArrayAdapter<String> mAdultsAdapter;
	private ArrayAdapter<String> mChildrenAdapter;
	
	private int mDepartCityInd = -1;
	private int mCountryInd = -1;
	private int mAdultInd = 1;
	private int mChildrenInd = 0;
	
	private EditText mEditSearchToursDepartCity;
	private EditText mEditSearchToursCountry;
	private EditText mEditSearchToursCity;
	private EditText mEditSearchToursHotelStars;
	private EditText mEditSearchToursDateFrom;
	private EditText mEditSearchToursDateTo;
	private EditText mEditSearchToursNightsFrom;
	private EditText mEditSearchToursNightsTo;
	private EditText mEditSearchToursHotel;
	private EditText mEditSearchToursAdults;
	private EditText mEditSearchToursChildren;
	
	private Button mButtonSearchToursSubmit;
	
	// Слушатель нажатия на поля с датами
	private final OnClickListener mEditDatesClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			DatePickerFragment fragment = new DatePickerFragment();
			String date = "";
			OnDateSetListener callback = null;
			
			switch (v.getId()) {
				case R.id.editSearchToursDateFrom:
					date = mEditSearchToursDateFrom.getText().toString();
					callback = mDateFromCallback;
					break;
				case R.id.editSearchToursDateTo:
					date = mEditSearchToursDateTo.getText().toString();
					callback = mDateToCallback;
					break;
			}
			
			String[] parts = date.split("\\.");
			Bundle args = new Bundle();
			
	    	if (parts.length == 3) {
				args.putInt("year", Integer.parseInt(parts[2]));
				args.putInt("month", Integer.parseInt(parts[1])-1);
				args.putInt("day", Integer.parseInt(parts[0]));
			}
			else {
				Calendar calendar = Calendar.getInstance();
				
				switch (v.getId()) {
					case R.id.editSearchToursDateFrom:
						calendar.add(Calendar.DAY_OF_MONTH, 1);
						break;
					case R.id.editSearchToursDateTo:
						calendar.add(Calendar.DAY_OF_MONTH, 10);
						break;
				}
				
				args.putInt("year", calendar.get(Calendar.YEAR));
				args.putInt("month", calendar.get(Calendar.MONTH));
				args.putInt("day", calendar.get(Calendar.DAY_OF_MONTH));
			}
			
			fragment.setArguments(args);
			fragment.setCallBack(callback);
			fragment.show(getFragmentManager(), "datePicker");
		}
	};
	
	// Слушатель нажатия на изменение поля "Дата вылета с"
    private OnDateSetListener mDateFromCallback = new OnDateSetListener() {
    	@Override
    	public void onDateSet(DatePicker view, int year, int month, int day) {
    		mEditSearchToursDateFrom.setText(buildDateString(year, month, day));
    	}
    };
    
	// Слушатель нажатия на изменение поля "Дата вылета по"
    private OnDateSetListener mDateToCallback = new OnDateSetListener() {
    	@Override
    	public void onDateSet(DatePicker view, int year, int month, int day) {
    		mEditSearchToursDateTo.setText(buildDateString(year, month, day));
    	}
    };
    
	// Слушатель нажатия на поля с требуемым диалогом
	private final OnClickListener mEditDialogClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			showEditDialog(v.getId());
		}
	};
	
	// Слушатель нажатия на поля с требуемым диалогом
	private final OnClickListener mEditMultiDialogClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			showEditMultiDialog(v.getId());
		}
	};
    
	// Слушатель нажатия на кнопку "Найти"
	private final OnClickListener mButtonSearchToursSubmitClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mEditSearchToursDepartCity.length() == 0) {
				Toast.makeText(getApplicationContext(), "Укажите город вылета для выполнения поиска туров!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (mEditSearchToursCountry.length() == 0) {
				Toast.makeText(getApplicationContext(), "Укажите страну прилёта для выполнения поиска туров!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			Calendar now = Calendar.getInstance();
			Calendar dateFrom = Calendar.getInstance();
			Calendar dateTo = Calendar.getInstance();
			
			now.set(Calendar.HOUR, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			
			String[] partsFrom = mEditSearchToursDateFrom.getText().toString().split("\\.");
			dateFrom.set(Integer.valueOf(partsFrom[2]), Integer.valueOf(partsFrom[1])-1, Integer.valueOf(partsFrom[0]));
			
			String[] partsTo = mEditSearchToursDateTo.getText().toString().split("\\.");
			dateTo.set(Integer.valueOf(partsTo[2]), Integer.valueOf(partsTo[1])-1, Integer.valueOf(partsTo[0]));
			
			if (dateTo.getTimeInMillis() < dateFrom.getTimeInMillis()) {
				Toast.makeText(getApplicationContext(), "Конечная дата вылета не может быть меньше начальной даты вылета!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (now.getTimeInMillis() > dateFrom.getTimeInMillis()) {
				Toast.makeText(getApplicationContext(), "Начальная дата вылета не может быть раньше сегодняшнего дня!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// Отправка поискового запроса на сервер			
			mCreateRequestFragment.execute(mCountriesIds.get(mCountryInd), mDepartCitiesIds.get(mDepartCityInd), getListChosenIds(mCitiesIds, mCitiesBools),
				getListChosenIds(mHotelStarsIds, mHotelStarsBools), getListChosenIds(mHotelsIds, mHotelsBools), Integer.toString(mAdultInd+1), Integer.toString(mChildrenInd),
				mEditSearchToursNightsFrom.getText().toString(), mEditSearchToursNightsTo.getText().toString(),
				mEditSearchToursDateFrom.getText().toString(), mEditSearchToursDateTo.getText().toString());
		}
	};
	
	private final GetCountriesFragment.Callbacks mGetCountriesCallbacks = new GetCountriesFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			// Чистим текущее поле
			mCountriesIds.clear();
			mCountriesAdapter.clear();
			
			mCountryInd = -1;
			mEditSearchToursCountry.setText("");
			
			// Чистим зависимые поля
			mCitiesBools.clear();
			mEditSearchToursCity.setText("");
			mEditSearchToursCity.setEnabled(false);
			
			mHotelStarsBools.clear();
			mEditSearchToursHotelStars.setText("");
			mEditSearchToursHotelStars.setEnabled(false);
			
			mHotelsBools.clear();
			mEditSearchToursHotel.setText("");
			mEditSearchToursHotel.setEnabled(false);
		}

		@Override
		public void onProgressUpdate(String... data) {
			mCountriesIds.add(data[0]);
			mCountriesAdapter.add(data[1]);
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				mEditSearchToursCountry.setEnabled(true);
			}
		}
	};
	
	private final GetCitiesFragment.Callbacks mGetCitiesCallbacks = new GetCitiesFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			// Чистим текущее поле
			mCitiesIds.clear();
			mCitiesNames.clear();
			
			mCitiesBools.clear();
			mEditSearchToursCity.setText("");
			
			// Ставим нестандартное значение
			mCitiesIds.add("0");
			mCitiesNames.add("Все курорты");
			mCitiesBools.add(false);
			
			// Чистим зависимые поля
			mHotelStarsBools.clear();
			mEditSearchToursHotelStars.setText("");
			mEditSearchToursHotelStars.setEnabled(false);
			
			mHotelsBools.clear();
			mEditSearchToursHotel.setText("");
			mEditSearchToursHotel.setEnabled(false);
		}

		@Override
		public void onProgressUpdate(String... data) {
			mCitiesIds.add(data[0]);
			mCitiesNames.add(data[1]);
			mCitiesBools.add(false);
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				mEditSearchToursCity.setEnabled(true);
			}
		}
	};
	
	private final GetHotelStarsFragment.Callbacks mGetHotelStarsCallbacks = new GetHotelStarsFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			// Чистим текущее поле
			mHotelStarsIds.clear();
			mHotelStarsNames.clear();
			
			mHotelStarsBools.clear();
			mEditSearchToursHotelStars.setText("");
			
			// Ставим нестандартное значение
			mHotelStarsIds.add("0");
			mHotelStarsNames.add("Все категории отелей");
			mHotelStarsBools.add(false);
			
			// Чистим зависимые поля
			mHotelsBools.clear();
			mEditSearchToursHotel.setText("");
			mEditSearchToursHotel.setEnabled(false);
		}

		@Override
		public void onProgressUpdate(String... data) {
			mHotelStarsIds.add(data[0]);
			mHotelStarsNames.add(data[1]);
			mHotelStarsBools.add(false);
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				mEditSearchToursHotelStars.setEnabled(true);
			}
		}
	};
	
	private final GetHotelsFragment.Callbacks mGetHotelsCallbacks = new GetHotelsFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			// Чистим текущее поле
			mHotelsIds.clear();
			mHotelsNames.clear();
			
			mHotelsBools.clear();
			mEditSearchToursHotel.setText("");
			
			// Ставим нестандартное значение
			mHotelsIds.add("0");
			mHotelsNames.add("Все отели");
			mHotelsBools.add(false);
		}

		@Override
		public void onProgressUpdate(String... data) {
			mHotelsIds.add(data[0]);
			mHotelsNames.add(data[1]);
			mHotelsBools.add(false);
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				mEditSearchToursHotel.setEnabled(true);
			}
		}
	};
	
	private final CreateRequestFragment.Callbacks mCreateRequestCallbacks = new CreateRequestFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialogHorizontal();
			
			mRequestId = 0;
		}

		@Override
		public void onProgressUpdate(String... data) {
			mRequestId = Integer.parseInt(data[0]);
		}

		@Override
		public void onPostExecute(String error) {
			if (error != null) {
				mProgressDialogHorizontal.dismiss();
				
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				mNumIterations = 20;
				mGetRequestStateFragment.execute(mRequestId, 0);
			}
		}
	};
	
	private final GetRequestStateFragment.Callbacks mGetRequestStateCallbacks = new GetRequestStateFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			mIsProcessedList = new ArrayList<Boolean>();
		}

		@Override
		public void onProgressUpdate(String... data) {
			mIsProcessedList.add(data[0].equals("true") ? true : false);
		}

		@Override
		public void onPostExecute(String error) {
			if (error != null) {
				mProgressDialogHorizontal.dismiss();
				
		 		Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				boolean ready = true;
				
				for (int i = 0; i < mIsProcessedList.size(); i++) {
					if (!mIsProcessedList.get(i)) {
						ready = false;
						break;
					}
				}
				
				mNumIterations--;
				
				if (ready) {
					mProgressDialogHorizontal.setProgress(100);
					mProgressDialogHorizontal.dismiss();
					
					Intent intent = new Intent(getApplicationContext(), SearchToursResultsActivity.class);
					intent.putExtra("request_id", mRequestId);
					startActivity(intent);
				}
				else {
					if (mNumIterations > 0) {
						mProgressDialogHorizontal.setProgress(100 - (mNumIterations * 5));
						
						mGetRequestStateFragment.execute(mRequestId, 2000);
					}
					else {
						mProgressDialogHorizontal.dismiss();
						
						Toast.makeText(getApplicationContext(), "Не удалось получить данные о турах за отведённый период времени.\nПопробуйте ещё раз.", Toast.LENGTH_SHORT).show();
					}
				}
			}
		}
	};
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_tours);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
	    // Подключаем фрагменты для потоков
	    FragmentManager fm = getFragmentManager();
	    mGetCountriesFragment = (GetCountriesFragment)fm.findFragmentByTag("countries_fragment");
	    mGetCitiesFragment = (GetCitiesFragment)fm.findFragmentByTag("cities_fragment");
	    mGetHotelStarsFragment = (GetHotelStarsFragment)fm.findFragmentByTag("hotel_stars_fragment");
	    mGetHotelsFragment = (GetHotelsFragment)fm.findFragmentByTag("hotels_fragment");
	    mCreateRequestFragment = (CreateRequestFragment)fm.findFragmentByTag("create_request_fragment");
	    mGetRequestStateFragment = (GetRequestStateFragment)fm.findFragmentByTag("get_request_state_fragment");
	    
	    if (mGetCountriesFragment == null) {
	    	mGetCountriesFragment = GetCountriesFragment.newInstance();
			fm.beginTransaction().add(mGetCountriesFragment, "countries_fragment").commit();
	    }
	    
	    if (mGetCitiesFragment == null) {
	    	mGetCitiesFragment = GetCitiesFragment.newInstance();
			fm.beginTransaction().add(mGetCitiesFragment, "cities_fragment").commit();
	    }
	    
	    if (mGetHotelStarsFragment == null) {
	    	mGetHotelStarsFragment = GetHotelStarsFragment.newInstance();
			fm.beginTransaction().add(mGetHotelStarsFragment, "hotel_stars_fragment").commit();
	    }
	    
	    if (mGetHotelsFragment == null) {
	    	mGetHotelsFragment = GetHotelsFragment.newInstance();
			fm.beginTransaction().add(mGetHotelsFragment, "hotels_fragment").commit();
	    }
	    
	    if (mCreateRequestFragment == null) {
	    	mCreateRequestFragment = CreateRequestFragment.newInstance();
			fm.beginTransaction().add(mCreateRequestFragment, "create_request_fragment").commit();
	    }
	    
	    if (mGetRequestStateFragment == null) {
	    	mGetRequestStateFragment = GetRequestStateFragment.newInstance();
			fm.beginTransaction().add(mGetRequestStateFragment, "get_request_state_fragment").commit();
	    }
	    
	    mGetCountriesFragment.setCallbacks(mGetCountriesCallbacks);
	    mGetCitiesFragment.setCallbacks(mGetCitiesCallbacks);
	    mGetHotelStarsFragment.setCallbacks(mGetHotelStarsCallbacks);
	    mGetHotelsFragment.setCallbacks(mGetHotelsCallbacks);
	    mCreateRequestFragment.setCallbacks(mCreateRequestCallbacks);
	    mGetRequestStateFragment.setCallbacks(mGetRequestStateCallbacks);
	    
	    // Инициализируем настройки
        mSettings = getSharedPreferences(MyDevice.APP_PREFS, Context.MODE_PRIVATE);
	    
	    // Город вылета
	    mEditSearchToursDepartCity = (EditText)findViewById(R.id.editSearchToursDepartCity);
	    mEditSearchToursDepartCity.setOnClickListener(mEditDialogClick);
	    
	    JSONArray citiesIds = null;
	    JSONArray citiesNames = null;
	    
	    try {
			citiesIds = new JSONArray(mSettings.getString(MyDevice.APP_PREFS_CITIES_IDS, null));
			citiesNames = new JSONArray(mSettings.getString(MyDevice.APP_PREFS_CITIES_NAMES, null));
		}
	    catch (JSONException e) {}
	    
	    mDepartCitiesIds = new ArrayList<String>();
    	mDepartCitiesAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
    	
    	if (citiesIds != null && citiesNames != null) {
    		for (int i = 0; i < citiesIds.length(); i++){ 
    			try {
					mDepartCitiesIds.add(citiesIds.getString(i));
					mDepartCitiesAdapter.add(citiesNames.getString(i));
				}
    			catch (JSONException e) {}
    		} 
    	}
	    
	    // Страна
	    mEditSearchToursCountry = (EditText)findViewById(R.id.editSearchToursCountry);
	    mEditSearchToursCountry.setOnClickListener(mEditDialogClick);
	    
	    mCountriesIds = new ArrayList<String>();
	    mCountriesAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
	    
	    // Курорт
	    mEditSearchToursCity = (EditText)findViewById(R.id.editSearchToursCity);
	    mEditSearchToursCity.setOnClickListener(mEditMultiDialogClick);
	    
	    mCitiesIds = new ArrayList<String>();
	    mCitiesNames = new ArrayList<String>();
	    mCitiesBools = new ArrayList<Boolean>();
	    
	    // Категория отеля
	    mEditSearchToursHotelStars = (EditText)findViewById(R.id.editSearchToursHotelStars);
	    mEditSearchToursHotelStars.setOnClickListener(mEditMultiDialogClick);
	    
	    mHotelStarsIds = new ArrayList<String>();
	    mHotelStarsNames = new ArrayList<String>();
	    mHotelStarsBools = new ArrayList<Boolean>();
	    
	    // Дата вылета с
	    Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
	    mEditSearchToursDateFrom = (EditText)findViewById(R.id.editSearchToursDateFrom);
	    mEditSearchToursDateFrom.setOnClickListener(mEditDatesClick);
	    mEditSearchToursDateFrom.setText(buildDateString(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
	    
	    // Дата вылета по
	    calendar.add(Calendar.DAY_OF_MONTH, 9);
	    mEditSearchToursDateTo = (EditText)findViewById(R.id.editSearchToursDateTo);
	    mEditSearchToursDateTo.setOnClickListener(mEditDatesClick);
	    mEditSearchToursDateTo.setText(buildDateString(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
	    
	    // Ночей с
	    mNightsAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
	 	for (int i = 0; i < 29; i++) {
	 		mNightsAdapter.add((i + 1) + "");
	 	}
	 	
	 	mEditSearchToursNightsFrom = (EditText)findViewById(R.id.editSearchToursNightsFrom);
	 	mEditSearchToursNightsFrom.setOnClickListener(mEditDialogClick);
	 	
	 	// Ночей по
	 	mEditSearchToursNightsTo = (EditText)findViewById(R.id.editSearchToursNightsTo);
	 	mEditSearchToursNightsTo.setOnClickListener(mEditDialogClick);
	 	
	    // Отель
	    mEditSearchToursHotel = (EditText)findViewById(R.id.editSearchToursHotel);
	    mEditSearchToursHotel.setOnClickListener(mEditMultiDialogClick);
	    
	    mHotelsIds = new ArrayList<String>();
	    mHotelsNames = new ArrayList<String>();
	    mHotelsBools = new ArrayList<Boolean>();
	 	
	 	// Взрослых
	 	mAdultsAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
	 	mAdultsAdapter.add("1 взрослый");
	 	mAdultsAdapter.add("2 взрослых");
	 	mAdultsAdapter.add("3 взрослых");
	 	mAdultsAdapter.add("4 взрослых");
	 	
	 	mEditSearchToursAdults = (EditText)findViewById(R.id.editSearchToursAdults);
	 	mEditSearchToursAdults.setOnClickListener(mEditDialogClick);
	 	
	 	// Детей
	 	mChildrenAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
	 	mChildrenAdapter.add("Без детей");
	 	mChildrenAdapter.add("1 ребёнок");
	 	mChildrenAdapter.add("2 детей");
	 	mChildrenAdapter.add("3 детей");
	 	
	 	mEditSearchToursChildren = (EditText)findViewById(R.id.editSearchToursChildren);
	 	mEditSearchToursChildren.setOnClickListener(mEditDialogClick);
	    
	    // Кнопка "Найти"
	    mButtonSearchToursSubmit = (Button)findViewById(R.id.buttonSearchToursSubmit);
	    mButtonSearchToursSubmit.setOnClickListener(mButtonSearchToursSubmitClick);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		NavUtils.navigateUpFromSameTask(this);
	    		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
	
	// Показ диалога потока
	private void showProgressDialogHorizontal() {
		mProgressDialogHorizontal = new ProgressDialog(this);
		mProgressDialogHorizontal.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialogHorizontal.setIndeterminate(false);
		mProgressDialogHorizontal.setMax(100);
		mProgressDialogHorizontal.setMessage("Производится поиск туров");
		mProgressDialogHorizontal.setCancelable(false);
		mProgressDialogHorizontal.show();
	}
	
	// Построение строки даты с ведущими нулями
	private String buildDateString(int year, int month, int day) {
		String sDay = Integer.toString(day);
  		String sMonth = Integer.toString(month+1);
		String sYear = Integer.toString(year);
  		
  		if (sDay.length() == 1) sDay = "0" + sDay;
  		if (sMonth.length() == 1) sMonth = "0" + sMonth;
  		
  		return sDay + "." + sMonth + "." + sYear;
	}
	
	// Показ диалога редактирования
	private void showEditDialog(final int id) {
		String title = "";
		int selectedIndex = -1;
		ArrayAdapter<String> adapter = null;
		
		switch (id) {
			case R.id.editSearchToursDepartCity:
				title = "Выберите город вылета";
				adapter = mDepartCitiesAdapter;
				selectedIndex = mDepartCityInd;
				break;
			case R.id.editSearchToursCountry:
				title = "Выберите страну прилёта";
				adapter = mCountriesAdapter;
				selectedIndex = mCountryInd;
				break;
			case R.id.editSearchToursNightsFrom:
				title = "Выберите начальное количество ночей";
				adapter = mNightsAdapter;
				selectedIndex = Integer.parseInt(mEditSearchToursNightsFrom.getText().toString()) - 1;
				break;
			case R.id.editSearchToursNightsTo:
				title = "Выберите конечное количество ночей";
				adapter = mNightsAdapter;
				selectedIndex = Integer.parseInt(mEditSearchToursNightsTo.getText().toString()) - 1;
				break;
			case R.id.editSearchToursAdults:
				title = "Выберите количество взрослых";
				adapter = mAdultsAdapter;
				selectedIndex = mAdultInd;
				break;
			case R.id.editSearchToursChildren:
				title = "Выберите количество детей";
				adapter = mChildrenAdapter;
				selectedIndex = mChildrenInd;
				break;
		}
		
		AlertDialog editDialog = new AlertDialog.Builder(SearchToursActivity.this)
    	.setTitle(title)
    	.setCancelable(true)
    	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int which) {
    			dialog.dismiss();
    		}
    	})
    	.setSingleChoiceItems(adapter, selectedIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	EditText view = null;
            	ArrayAdapter<String> adapter = null;
            	
            	switch (id) {
            		case R.id.editSearchToursDepartCity:
            			view = mEditSearchToursDepartCity;
            			adapter = mDepartCitiesAdapter;
            			
            			mDepartCityInd = which;
            			break;
    				case R.id.editSearchToursCountry:
    					view = mEditSearchToursCountry;
    					adapter = mCountriesAdapter;
    					
    					mCountryInd = which;
    					break;
    				case R.id.editSearchToursNightsFrom:
    					view = mEditSearchToursNightsFrom;
    					adapter = mNightsAdapter;
    					break;
    				case R.id.editSearchToursNightsTo:
    					view = mEditSearchToursNightsTo;
    					adapter = mNightsAdapter;
    					break;
    				case R.id.editSearchToursAdults:
    					view = mEditSearchToursAdults;
    					adapter = mAdultsAdapter;
    					
    					mAdultInd = which;
    					break;
    				case R.id.editSearchToursChildren:
    					view = mEditSearchToursChildren;
    					adapter = mChildrenAdapter;
    					
    					mChildrenInd = which;
    					break;
            	}
            	
            	view.setText(adapter.getItem(which));
            	dialog.dismiss();
            	
            	switch (id) {
        			case R.id.editSearchToursDepartCity:
        				mGetCountriesFragment.execute(mDepartCitiesIds.get(mDepartCityInd));
        				break;
        			case R.id.editSearchToursCountry:
        				mGetCitiesFragment.execute(mCountriesIds.get(mCountryInd));
        				break;
            	}
            }
        })
    	.create();
	
		editDialog.show();
	}
	
	// Показ диалога редактирования с мультивыбором
	private void showEditMultiDialog(final int id) {
		String title = "";
		ArrayList<Boolean> checkedItems = null;
		CharSequence[] adapter = null;
		
		switch (id) {
			case R.id.editSearchToursCity:
				title = "Выберите курорты";
				adapter = mCitiesNames.toArray(new CharSequence[mCitiesNames.size()]);
				checkedItems = mCitiesBools;
				break;
			case R.id.editSearchToursHotelStars:
				title = "Выберите категории отеля";
				adapter = mHotelStarsNames.toArray(new CharSequence[mHotelStarsNames.size()]);
				checkedItems = mHotelStarsBools;
				break;
			case R.id.editSearchToursHotel:
				title = "Выберите отели";
				adapter = mHotelsNames.toArray(new CharSequence[mHotelsNames.size()]);
				checkedItems = mHotelsBools;
				break;
		}
		
		final boolean[] checked = new boolean[checkedItems.size()];
		for (int i = 0; i < checkedItems.size(); i++) {
			checked[i] = checkedItems.get(i);
		}
		
		AlertDialog editMultiDialog = new AlertDialog.Builder(SearchToursActivity.this)
    	.setTitle(title)
    	.setCancelable(true)
    	.setMultiChoiceItems(adapter, checked, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            	if (which == 0) {
            		if (isChecked) {
            			for (int i = 0; i < checked.length; i++) {
            				checked[i] = true;
            				((AlertDialog)dialog).getListView().setItemChecked(i, true);
            			}
            		}
            		else {
            			for (int i = 0; i < checked.length; i++) {
            				checked[i] = false;
            				((AlertDialog)dialog).getListView().setItemChecked(i, false);
            			}
            		}
            	}
            	else {
            		if (!isChecked) {
            			if (checked[0]) {
            				checked[0] = false;
            				((AlertDialog)dialog).getListView().setItemChecked(0, false);
            			}
            		}
            	}
            }
        })
    	.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int which) {
    			dialog.dismiss();
    		}
    	})
    	.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
    		public void onClick(DialogInterface dialog, int which) {
            	EditText view = null;
            	ArrayList<String> adapter = null;
            	ArrayList<Boolean> checkedItems = null;
            	
            	switch (id) {
            		case R.id.editSearchToursCity:
    					view = mEditSearchToursCity;
    					adapter = mCitiesNames;
    					checkedItems = mCitiesBools;
    					break;
            		case R.id.editSearchToursHotelStars:
    					view = mEditSearchToursHotelStars;
    					adapter = mHotelStarsNames;
    					checkedItems = mHotelStarsBools;
    					break;
            		case R.id.editSearchToursHotel:
    					view = mEditSearchToursHotel;
    					adapter = mHotelsNames;
    					checkedItems = mHotelsBools;
    					break;
            	}
    			
            	view.setText("");
            	String text = "";
            	String textAll = "";
            	
            	for (int i = 0; i < checked.length; i++) {
            		if (i == 0 && checked[i]) {
            			textAll = adapter.get(i);
            		}
            		
            		checkedItems.set(i, checked[i]);
    				
    				if (checked[i]) {
    					text += adapter.get(i) + ", ";
    				}
    			}
    			
            	if (textAll.length() > 0) {
            		view.setText(textAll);
            	}
            	else if (text.length() >= 2) {
            		view.setText(text.substring(0, text.length() - 2));
            	}
            	else {
            		checked[0] = true;
            		checkedItems.set(0, true);
            		view.setText(view.getHint());
            	}
            	
    			dialog.dismiss();
    			
            	switch (id) {
        			case R.id.editSearchToursCity:
        				mGetHotelStarsFragment.execute(mCountriesIds.get(mCountryInd), getListChosenIds(mCitiesIds, mCitiesBools));
        				break;
        			case R.id.editSearchToursHotelStars:
        				mGetHotelsFragment.execute(mCountriesIds.get(mCountryInd), getListChosenIds(mCitiesIds, mCitiesBools), getListChosenIds(mHotelStarsIds, mHotelStarsBools));
        				break;
            	}
    		}
    	})
    	.create();
	
		editMultiDialog.show();
	}
	
	
	// Получение списка выбранных кодов
	private ArrayList<Integer> getListChosenIds(ArrayList<String> listIds, ArrayList<Boolean> listBools) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for (int i = 0; i < listIds.size(); i++) {
			if (listBools.size() == 0) break;
			
			if (listBools.get(i)) {
				if (i == 0) break;
				list.add(Integer.parseInt(listIds.get(i)));
			}
		}
		
		return list;
	}
}