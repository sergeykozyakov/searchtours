package ru.ostw.SearchTours;

import java.util.ArrayList;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class SearchToursResultsActivity extends FragmentListActivity {
	
	private ProgressDialog mProgressDialog;
	
	private GetRequestResultFragment mGetRequestResultFragment;
	private ActualizePriceFragment mActualizePriceFragment;
	
	private ToursAdapter mListAdapter;
	private Button mButtonSearchToursResultsOrder;
	
	private int mRequestId = 0;
	private String[] mTourData;
	
	// Слушатель нажатия на кнопку "Заказать тур"
	private final OnClickListener mButtonSearchToursResultsOrderClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(new Intent(getApplicationContext(), OrderTourActivity.class));
		}
	};
	
	private final GetRequestResultFragment.Callbacks mGetRequestResultCallbacks = new GetRequestResultFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			mListAdapter.clear();
			mButtonSearchToursResultsOrder.setVisibility(View.GONE);
		}

		@Override
		public void onProgressUpdate(Tour... data) {
			mListAdapter.add(data[0]);
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (mListAdapter.getCount() == 0) {
				mButtonSearchToursResultsOrder.setVisibility(View.VISIBLE);
			}
			
			if (error != null) {
				Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
		}
	};
	
	private final ActualizePriceFragment.Callbacks mActualizePriceCallbacks = new ActualizePriceFragment.Callbacks() {
		@Override
		public void onPreExecute() {
			showProgressDialog();
			
			mTourData = null;
		}

		@Override
		public void onProgressUpdate(String... data) {
			mTourData = data;
		}

		@Override
		public void onPostExecute(String error) {
			mProgressDialog.dismiss();
			
			if (error != null) {
				Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
		 	}
			else {
				Intent intent = new Intent(getApplicationContext(), TourCardActivity.class);
				intent.putExtra("tour_data", mTourData);
				startActivity(intent);
			}
		}
	};
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_tours_results);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Подключаем фрагменты для потоков
	    FragmentManager fm = getFragmentManager();
	    mGetRequestResultFragment = (GetRequestResultFragment)fm.findFragmentByTag("request_result_fragment");
	    mActualizePriceFragment = (ActualizePriceFragment)fm.findFragmentByTag("actualize_price_fragment");
	    
	    if (mGetRequestResultFragment == null) {
	    	mGetRequestResultFragment = GetRequestResultFragment.newInstance();
			fm.beginTransaction().add(mGetRequestResultFragment, "request_result_fragment").commit();
	    }
	    
	    if (mActualizePriceFragment == null) {
	    	mActualizePriceFragment = ActualizePriceFragment.newInstance();
			fm.beginTransaction().add(mActualizePriceFragment, "actualize_price_fragment").commit();
	    }
	    
	    mGetRequestResultFragment.setCallbacks(mGetRequestResultCallbacks);
	    mActualizePriceFragment.setCallbacks(mActualizePriceCallbacks);
	    
	    Intent intent = getIntent();
    	mRequestId = intent.getIntExtra("request_id", 0);
	    
	    mListAdapter = new ToursAdapter(this, R.layout.list_item_tour, new ArrayList<Tour>());
	    setListAdapter(mListAdapter);
	    
	    mButtonSearchToursResultsOrder = (Button)findViewById(R.id.buttonSearchToursResultsOrder);
	    mButtonSearchToursResultsOrder.setOnClickListener(mButtonSearchToursResultsOrderClick);
	    
	    mGetRequestResultFragment.execute(Integer.toString(mRequestId));
	}

	@Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_search_tours_results;
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
		mActualizePriceFragment.execute(Integer.toString(((Tour)l.getItemAtPosition(position)).sourceId), 
			Integer.toString(((Tour)l.getItemAtPosition(position)).offerId), Integer.toString(mRequestId));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case android.R.id.home:
	    		onBackPressed();
	    		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	
	// Показ диалога потока
	private void showProgressDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Пожалуйста, подождите");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}
}