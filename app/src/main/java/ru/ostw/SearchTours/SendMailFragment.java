package ru.ostw.SearchTours;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

public class SendMailFragment extends Fragment {
	
	private static final String SMTP_LOGIN = "ostw.app@gmail.com";
	private static final String SMTP_PASSWORD = "ostwapp123";
	private static final String SMTP_RECIPIENTS = "ta7.ekaterinburg@club.coral.ru";
	
	private Callbacks mCallbacks;
	private EmailTask mTask;
	
	public static interface Callbacks {
		void onPreExecute();
		void onProgressUpdate();
		void onPostExecute(String error);
	}
	
	public static SendMailFragment newInstance() {
        return new SendMailFragment();
    }
	
	public void setCallbacks(Callbacks callbacks) {
		mCallbacks = callbacks;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mCallbacks = null;
	}
	
	public void execute(String... params) {
		mTask = new EmailTask();
		mTask.execute(params);
	}
	
	public boolean isTaskRunning() {
		if (mTask != null) {
			if (!mTask.isCancelled() && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				return true;
			}
		}
		return false;
	}
	
	private class EmailTask extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (mCallbacks != null) {
				mCallbacks.onPreExecute();
			}
		}
		
		@Override
		protected String doInBackground(String... params) {
			try {
                GMailSender sender = new GMailSender(SMTP_LOGIN, SMTP_PASSWORD);
                sender.sendMail(params[0], params[1], SMTP_LOGIN, SMTP_RECIPIENTS);
            }
			catch (Exception e) {   
				return "Не удалось отправить данные в турагентство!";
            }
	        
			return null;
		}
		
	    @Override
	    protected void onProgressUpdate(Void... data) {
	    	super.onProgressUpdate(data);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onProgressUpdate();
	    	}
	    }
	    
	    @Override
	    protected void onPostExecute(String error) {
	    	super.onPostExecute(error);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onPostExecute(error);
	    	}
	    }
	}
}