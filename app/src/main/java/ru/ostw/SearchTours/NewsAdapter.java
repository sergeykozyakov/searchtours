package ru.ostw.SearchTours;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class NewsAdapter extends ArrayAdapter<HashMap<String, String>> {
    private Context context;
    private int layoutResourceId;
    private ArrayList<HashMap<String, String>> data;
    
    private ImageLoader imageLoader; 
     
    public NewsAdapter(Context context, int layoutResourceId, ArrayList<HashMap<String, String>> data) {
    	super(context, layoutResourceId, data);
    	
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
       
        imageLoader = new ImageLoader(context);
    }
     
    public View getView(int position, View convertView, ViewGroup parent) {
    	View row = convertView;
		NewsHolder holder = null;
		
		if (row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new NewsHolder();
			holder.imageNews = (ImageView)row.findViewById(R.id.imageNews);
			holder.textNewsTitle = (TextView)row.findViewById(R.id.textNewsTitle);
			holder.textNewsDate = (TextView)row.findViewById(R.id.textNewsDate);
			holder.textNewsType = (TextView)row.findViewById(R.id.textNewsType);

			row.setTag(holder);
		}
		else {
			holder = (NewsHolder)row.getTag();
		}
        
        HashMap<String, String> news = new HashMap<String, String>();
        news = data.get(position); 
        
        String spaces = "";
        if (news.get("Date").length() > 0) {
        	spaces = "   ";
        }
        
        imageLoader.DisplayImage(news.get("Image"), holder.imageNews);
        holder.textNewsTitle.setText(news.get("Header"));
        holder.textNewsDate.setText(news.get("Date") + spaces);
        holder.textNewsType.setText(news.get("Type"));
        
        return row;
    }
    
	private static class NewsHolder {
		ImageView imageNews;
		TextView textNewsTitle;
        TextView textNewsDate;
        TextView textNewsType;
	}
}