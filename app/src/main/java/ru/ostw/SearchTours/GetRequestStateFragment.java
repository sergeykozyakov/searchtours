package ru.ostw.SearchTours;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

public class GetRequestStateFragment extends Fragment {
	
	private static final String SOAP_ACTION = "urn:SletatRu:Contracts:Soap11Gate:v1/Soap11Gate/GetRequestState";
	private static final String METHOD_NAME = "GetRequestState";
	private static final String METHOD_NAME_RESULT = "GetRequestStateResult";
	private static final String PROPERTY_REQUEST_ID = "requestId";
	private static final String PROPERTY_IS_PROCESSED = "IsProcessed";
	
	private Callbacks mCallbacks;
	private HttpTask mTask;
	
	public static interface Callbacks {
		void onPreExecute();
		void onProgressUpdate(String... data);
		void onPostExecute(String error);
	}
	
	public static GetRequestStateFragment newInstance() {
        return new GetRequestStateFragment();
    }
	
	public void setCallbacks(Callbacks callbacks) {
		mCallbacks = callbacks;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRetainInstance(true);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mCallbacks = null;
	}
	
	public void execute(Integer... params) {
		mTask = new HttpTask();
		mTask.execute(params);
	}
	
	public boolean isTaskRunning() {
		if (mTask != null) {
			if (!mTask.isCancelled() && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				return true;
			}
		}
		return false;
	}
	
	private class HttpTask extends AsyncTask<Integer, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if (mCallbacks != null) {
				mCallbacks.onPreExecute();
			}
		}
		
		@Override
		protected String doInBackground(Integer... params) {
			try {
				Thread.sleep(params[1]);
			}
			catch (InterruptedException e) {}
			
			SoapObject request = new SoapObject(SoapAuth.NAMESPACE, METHOD_NAME);
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			
			envelope.headerOut = new Element[1];
			envelope.headerOut[0] = SoapAuth.buildAuthHeader();
			
			request.addProperty(PROPERTY_REQUEST_ID, Integer.toString(params[0]));
			
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(SoapAuth.URL);
			
			try {
				androidHttpTransport.call(SOAP_ACTION, envelope);
				
		        SoapObject resultsRequestSOAP = (SoapObject)envelope.bodyIn;
				SoapObject result = (SoapObject)resultsRequestSOAP.getProperty(METHOD_NAME_RESULT);
				
				for (int i = 0; i < result.getPropertyCount(); i++) {
					SoapObject resultRow = (SoapObject)result.getProperty(i);
					publishProgress(resultRow.getProperty(PROPERTY_IS_PROCESSED).toString());
				}
			}
			catch (Exception e) {
				return "Не удалось получить статус поиска туров!";
			}
	        
			return null;
		}
		
	    @Override
	    protected void onProgressUpdate(String... data) {
	    	super.onProgressUpdate(data);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onProgressUpdate(data);
	    	}
	    }
	    
	    @Override
	    protected void onPostExecute(String error) {
	    	super.onPostExecute(error);
	    	
	    	if (mCallbacks != null) {
	    		mCallbacks.onPostExecute(error);
	    	}
	    }
	}
}