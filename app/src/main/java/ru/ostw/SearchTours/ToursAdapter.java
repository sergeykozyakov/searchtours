package ru.ostw.SearchTours;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ToursAdapter extends ArrayAdapter<Tour> {
	private Context context; 
	private int layoutResourceId;    
	private ArrayList<Tour> data = null;

	public ToursAdapter(Context context, int layoutResourceId, ArrayList<Tour> data) {
		super(context, layoutResourceId, data);
		
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		View row = convertView;
		ToursHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new ToursHolder();
			holder.textResortName = (TextView)row.findViewById(R.id.textTourResortName);
			holder.textHotelName = (TextView)row.findViewById(R.id.textTourHotelName);
			holder.textStarName = (TextView)row.findViewById(R.id.textTourStarName);
			holder.textCheckInDate = (TextView)row.findViewById(R.id.textTourCheckInDate);
			holder.textPrice = (TextView)row.findViewById(R.id.textTourPrice);
			
			holder.imageStar1 = (ImageView)row.findViewById(R.id.imageTourStar1);
			holder.imageStar2 = (ImageView)row.findViewById(R.id.imageTourStar2);
			holder.imageStar3 = (ImageView)row.findViewById(R.id.imageTourStar3);
			holder.imageStar4 = (ImageView)row.findViewById(R.id.imageTourStar4);
			holder.imageStar5 = (ImageView)row.findViewById(R.id.imageTourStar5);

			row.setTag(holder);
		}
		else {
			holder = (ToursHolder)row.getTag();
		}

		holder.imageStar1.setVisibility(View.GONE);
		holder.imageStar2.setVisibility(View.GONE);
		holder.imageStar3.setVisibility(View.GONE);
		holder.imageStar4.setVisibility(View.GONE);
		holder.imageStar5.setVisibility(View.GONE);
		
		holder.textStarName.setVisibility(View.GONE);
		
		Tour tour = data.get(position);
		
		String nights = "ночей";
		if (tour.nights.endsWith("1") && !tour.nights.endsWith("11")) {
			nights = "ночь";
		}
		else if (tour.nights.endsWith("2") && !tour.nights.endsWith("12")) {
			nights = "ночи";
		}
		else if (tour.nights.endsWith("3") && !tour.nights.endsWith("13")) {
			nights = "ночи";
		}
		else if (tour.nights.endsWith("4") && !tour.nights.endsWith("14")) {
			nights = "ночи";
		}
		
		if (tour.starName.contains("1")) {
			holder.imageStar1.setVisibility(View.VISIBLE);
		}
		else if (tour.starName.contains("2")) {
			holder.imageStar1.setVisibility(View.VISIBLE);
			holder.imageStar2.setVisibility(View.VISIBLE);
		}
		else if (tour.starName.contains("3")) {
			holder.imageStar1.setVisibility(View.VISIBLE);
			holder.imageStar2.setVisibility(View.VISIBLE);
			holder.imageStar3.setVisibility(View.VISIBLE);
		}
		else if (tour.starName.contains("4")) {
			holder.imageStar1.setVisibility(View.VISIBLE);
			holder.imageStar2.setVisibility(View.VISIBLE);
			holder.imageStar3.setVisibility(View.VISIBLE);
			holder.imageStar4.setVisibility(View.VISIBLE);
		}
		else if (tour.starName.contains("5")) {
			holder.imageStar1.setVisibility(View.VISIBLE);
			holder.imageStar2.setVisibility(View.VISIBLE);
			holder.imageStar3.setVisibility(View.VISIBLE);
			holder.imageStar4.setVisibility(View.VISIBLE);
			holder.imageStar5.setVisibility(View.VISIBLE);
		}
		else {
			holder.textStarName.setVisibility(View.VISIBLE);
		}
		
		holder.textResortName.setText(tour.countryName + ", " + tour.resortName);
		holder.textHotelName.setText(tour.hotelName);
		holder.textStarName.setText(tour.starName);
		holder.textCheckInDate.setText(tour.nights + " " + nights + ", вылет " + tour.checkInDate);
		holder.textPrice.setText(tour.price + " руб.");

		return row;
	}

	private static class ToursHolder {
		TextView textResortName;
		TextView textHotelName;
		TextView textStarName;
		TextView textCheckInDate;
		TextView textPrice;
		
		ImageView imageStar1;
		ImageView imageStar2;
		ImageView imageStar3;
		ImageView imageStar4;
		ImageView imageStar5;
	}
}